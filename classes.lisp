(in-package #:noctool)

(defvar *dont-muck-with-instance* NIL "don't do init magic on objects that might be saved... assume this will be done elsewhere")

;; a hash so we can look up objects by their ID
(defvar *id-objects* (make-hash-table :test #'equal))

(defclass id-object ()
  ((id :reader id :initarg :id)))

(defmethod initialize-instance :after ((instance id-object) &key)
  (setf (gethash (symbol-name (id instance)) *id-objects*)
        instance))

(defmacro get-instance-by-id (id-string)
  `(gethash ,id-string *id-objects*))

(defclass parented-object ()
  ((parent :accessor parent :initform NIL :initarg :parent)))

(defclass alert-level ()
  ((alert-level :accessor alert-level :initarg :alert-level :initform 0)))

(defclass equipment (id-object alert-level)
  ((monitors :accessor monitors :initarg :monitors :initform nil)
   (name :reader name :initarg :name)
   (address :reader address :initarg :address)
   (username :reader username :initarg :username :initform nil) 
   (proxies :accessor proxies :initarg :proxies :initform nil))
  (:default-initargs :id (gentemp "EQ-" (find-package :noctool-symbols))))


(defclass proxy ()
  ((remote-node :reader remote-node :initarg :remote-node)
   (object :reader object :initarg :object) 
   ))

(defclass monitor (id-object parented-object alert-level)
  ((equipment :reader equipment :initarg :equipment)
   (interval :accessor interval :initarg :interval :initform 300)
   (last-updated :accessor last-updated :initarg :last-updated :initform 0)
   (proxies :accessor proxies :initform nil)
   )
  (:default-initargs :id (gentemp "MON-" (find-package :noctool-symbols))))

(defclass cpu-monitor (monitor)
  ((num :reader num :initarg :num :initform 0)
   )
  (:default-initargs :interval 300))

(defclass load-monitor (monitor)
  ((low-water :reader low-water :initarg :low-water)
   (high-water :reader high-water :initarg :high-water) 
   (one-min :reader one-min :initarg :one-min)
   (five-min :reader five-min :initarg :five-min)
   (ten-min :reader ten-min :initarg :ten-min)
   (graph-1 :reader graph-1 :initarg :graph-1)
   (graph-5 :reader graph-5 :initarg :graph-5)
   (graph-10 :reader graph-10 :initarg :graph-10)
   )
  (:default-initargs :low-water 1.0 :high-water 5.0))

(defclass tcp-monitor (monitor)
  ((sent-data :reader sent-data :initarg :sent-data :initform nil)
   (match-data :reader match-data :initarg :match-data :initform nil)
   (port :reader port :initarg :port)
   (delay :reader delay :initarg :delay) 
   ))

(defclass http-monitor (tcp-monitor)
  ((url :reader url :initarg :url)
   (protocol :reader protocol :initarg :protocol)
   (servername :reader servername :initarg :servername)
   (request-method :reader request-method :initarg :request-method) 
   )
  (:default-initargs :protocol 1.0 :servername nil :request-method "GET" :url "/" :match-data "200 OK" :port 80 :delay 0.1))

(defmethod sent-data ((mon http-monitor))
  (if (>= (protocol mon) 1.0)
      (format nil "~a ~a HTTP/~3,1f~c~c~{~a~}~c~c"
	      (request-method mon)
	      (url mon)
	      (protocol mon)
	      (code-char 13)(code-char 10)
	      (when (servername mon)
		(format nil "Host:~a~c~c" (servername mon) (code-char 13)(code-char 10)))
	      (code-char 13)(code-char 10))
    (format nil "~a ~a~c~c~c~c" (request-method mon) (url mon) (code-char 13)(code-char 10)(code-char 13)(code-char 10))))

(add-graph-info load-monitor graph-1 avg-graph percentile-graph-display)
(add-graph-info load-monitor graph-5 avg-graph percentile-graph-display)
(add-graph-info load-monitor graph-10 avg-graph percentile-graph-display)

(defclass ping-monitor (monitor)
  ((max-fail :reader max-fail :initarg :max-fail :initform 1)
   (max-rtt :reader max-rtt :initarg :max-rtt :initform 20)
   (failed :accessor failed :initarg :failed :initform 0)
   (over-rtt :accessor over-rtt :initarg :over-rtt :initform 0)
   (ping-count :reader ping-count :initarg :ping-count :initform 5)
   (graph :reader graph :initarg :graph :initform nil)
   (ping-interval :reader ping-interval :initarg :ping-interval 
              :initform (if (eql 0 (sb-posix:geteuid)) 0.1 1)))
  (:default-initargs :interval 60))

(defmethod initialize-instance :after ((instance ping-monitor) &key)
  (unless *dont-muck-with-instance* (add-graphs instance)))

(add-graph-info ping-monitor graph gauge-graph percentile-graph-display)

(defclass disk-monitor (monitor)
  ((device :reader device :initarg :device) 
   (disk-max :accessor disk-max :initarg :disk-max :initform 0)
   (inodes-max :accessor inodes-max :initarg :inodes-max :initform 0)
   (disk-free :accessor disk-free :initarg :disk-free :initform 0)
   (inodes-free :accessor inodes-free :initarg :inodes-free :initform 0)
   (disk-percent :reader disk-percent :initarg :disk-percent)
   (inodes-percent :reader inodes-percent :initarg :inodes-percent)
   (mountpoint :accessor mountpoint :initarg :mountpoint)
   (disk-graph :reader disk-graph :initarg :disk-graph :initform nil)
   (inode-graph :reader inode-graph :initarg :inode-graph :initform nil)
   (last-seen :accessor last-seen :initarg :last-seen :initform 0)
   (max-since-seen :accessor max-since-seen)
   )
  (:default-initargs :interval 600 :disk-percent 90 :inodes-percent 90))

(add-graph-info disk-monitor disk-graph max-graph percentile-graph-display)
(add-graph-info disk-monitor inode-graph max-graph percentile-graph-display)

(defclass disk-container (monitor)
  ((disk-list :accessor disk-list :initarg :disk-list :initform nil)
   (ignore-list :reader ignore-list :initarg :ignore-list)
   (max-since-seen :accessor max-since-seen))
  (:default-initargs :interval 600 :ignore-list (list "tmpfs")))

(defmethod max-since-seen :around ((mon disk-monitor))
  (if (slot-boundp mon 'max-since-seen)
      (call-next-method)
      (max-since-seen (parent mon))))

(defmethod max-since-seen :around ((mon disk-container))
  (if (slot-boundp mon 'max-since-seen)
      (call-next-method)
      (* (interval mon) 2)))

(defmethod initialize-instance :after ((instance disk-container) &key)
  (unless *dont-muck-with-instance*
    ;; set the alert-level to the max of the children
    (setf (alert-level instance)
          (reduce #'max (disk-list instance) :key 'alert-level :initial-value 0))))

(defclass process-container (monitor)
  ((processes :accessor processes :initarg processes :initform NIL)))

(defun find-all (item sequence &rest args &key (start 0) end key (test #'eql) test-not)
  (declare (ignorable test-not item args))
  (loop for x in (subseq sequence start end)
       when (apply test (if key (apply key x) x))
       collect x))


(defclass process-monitor (monitor)
  ((name :reader name :initarg :name :initform (error "proc monitor must have a name!"))
   (owner :reader owner :initarg :owner :initform NIL)
   (pid :reader pid :initarg :pid :initform NIL)
   (ppid :reader ppid :initarg :ppid :initform NIL)
   (args :reader args :initarg :args :initform NIL)
   (min-procs :reader min-procs :initarg :min :initform 1)
   (max-procs :reader max-procs :initarg :max :initform 1)
   (found-procs :accessor found-procs :initform 0)))

(defclass host (equipment)
  ((passwd :reader passwd :initarg :passwd)
   (ssh-port :reader ssh-port :initarg :ssh-port :initform 22) 
   ))

(defclass unix-host (host)
  ())

(defclass linux-host (unix-host)
  ())

(defclass macos-host (unix-host)
  ())

(defclass snmp-container (monitor)
  ((interfaces :accessor interfaces :initarg :interfaces)
   (version :accessor version :initarg :version)
   (public :accessor public :initarg :public)
   (private :accessor private :initarg :private) 
   )
  (:default-initargs :version :v2c :public "public"
		     :private "private" :interfaces nil))

(defclass snmp-interface (monitor)
  ((name :reader name :initarg :name)
   (in-octets :accessor in-octets :initarg :in-octets)
   (out-octets :accessor out-octets :initarg :out-octets)
   (in-errors :accessor in-errors :initarg :in-errors)
   (out-errors :accessor out-errors :initarg :out-errors)
   (in-discards :accessor in-discards :initarg :in-discards)
   (out-discards :accessor out-discards :initarg :out-discards) 
   (admin-status :accessor admin-status :initarg :admin-status)
   (oper-status :accessor oper-status :initarg :oper-status))
  (:default-initargs :admin-status nil :oper-status nil))

(defmethod initialize-instance :after ((instance snmp-interface) &key)
  (unless *dont-muck-with-instance* (add-graphs instance)))

(add-graph-info snmp-interface in-octets meter-graph percentile-graph-display)
(add-graph-info snmp-interface out-octets meter-graph percentile-graph-display)
(add-graph-info snmp-interface in-errors meter-graph percentile-graph-display)
(add-graph-info snmp-interface out-errors meter-graph percentile-graph-display)
(add-graph-info snmp-interface in-discards meter-graph percentile-graph-display)
(add-graph-info snmp-interface out-discards meter-graph percentile-graph-display)

(defun make-snmp-interface (ifname)
  (make-instance 'snmp-interface :name ifname))

(defmethod store ((graph disk-monitor) &optional filename)
  (labels ((fname (subtype) (merge-pathnames
			     (make-pathname :name (format nil "~a%~a" filename subtype))
			     *storage-path*)))
    
    (list
     (store (disk-graph graph) (fname "disk"))
     (store (inode-graph graph) (fname "inode")))))


(defmethod store ((graph disk-container) &optional filename)
  (declare (ignore filename))
  (let* ((basename (format nil "~a%disk" (name (equipment graph))))
	 (filename (merge-pathnames (make-pathname :name basename)
				   *storage-path*)))
    
    (with-open-file (stream filename
			    :direction :output
			    :if-exists :supersede
			    :if-does-not-exist :create)
      (format stream "(let ((diskmon (make-instance 'disk-container :equipment *equipment* :ignore-list '~s :interval ~a)))~%" (ignore-list graph) (interval graph))
      (loop for disk in (disk-list graph)
	    for n from 1
	    do (let* ((basename (format nil "~a%~d" basename n))
		      (files (store disk basename)))
		 (format stream "  (let ((*disk* (make-instance 'disk-monitor :device ~s :equipment (equipment diskmon) :disk-max ~a :interval (interval diskmon))))~%" (device disk) (disk-max disk))
		 (loop for file in files
		       do (format stream "    (let ((*graph* (~a *disk*)))~%      (load ~s))~%" (cond ((search "%disk%disk" (pathname-name file)) "disk-graph") ((search "%disk%inode" (pathname-name file)) "inode-graph")) file)))
	    (format stream "  (push *disk* (disk-list diskmon)))~%"))
      (format stream "  (push diskmon (monitors *equipment*)))~%"))
    (list filename)))

(defmethod store ((graph equipment) &optional filename)
  (declare (ignore filename))
  (let ((files (mapcan 'store (monitors graph))))
    (with-open-file (stream
		     (merge-pathnames
			     (make-pathname :name (name graph))
			     *storage-path*)
		     :direction :output
		     :if-exists :supersede :if-does-not-exist :create)
      (format stream "(let ((*equipment*
       (make-instance '~a~%
		      :name ~s
		      :address ~s" (class-name (class-of graph)) (name graph) (address graph))
      (when (username graph)
	  (format stream "~%            :username ~s" (username graph)))
      (format stream ")))~%")
      (loop for file in files
	    do (format stream "  (load ~s)~%" file))
      (format stream "(push *equipment* *monitors*))"))))

(defclass remote-node ()
  ((destination :reader destination :initarg :destination)
   (dst-port :reader dst-port :initarg :dst-port)
   (my-name :reader my-name :initarg :my-name)
   (my-passwd :reader my-passwd :initarg :my-passwd)
   (remote-passwd :reader remote-passwd :initarg :remote-passwd)
   (conn :accessor conn :initarg :conn)
   )
  (:default-initargs :dst-port 11378 :conn nil))

(defmethod remote-passwd ((peer null))
  (octetify "a"))

(defclass view ()
  ((width :accessor width :initarg :width)
   (height :accessor height :initarg :height)
   (name :accessor name :initarg :name)
   (display-objects :accessor display-objects :initarg :display-objects)
   )
  (:default-initargs :display-objects nil))

(defgeneric post-config-fixup (object))
(defmethod post-config-fixup (object)
  ;; Default do-naught method
  (values))

(defmethod post-config-fixup ((instance load-monitor))
  (unless *dont-muck-with-instance*
    (add-graphs instance)))

;; if an instance has a name, but no address, give it an address
;; if an instance has an address, but no name, give it a name
;; if it has neither, signal an error
(defmethod post-config-fixup ((instance equipment))
  ;; set the alert-level to the max of the children
  (setf (alert-level instance)
        (reduce #'max (monitors instance) :key 'alert-level :initial-value 0))
  ;; make sure the name and address are bound
  (unless *dont-muck-with-instance*
    (cond ((and (not (slot-boundp instance 'address))
              (not (slot-boundp instance 'name)))
         (error "both name and address are unbound for this host!"))
        ((not (slot-boundp instance 'address))
         (setf (slot-value instance 'address)
               (let ((hostent 
                      (sb-bsd-sockets:host-ent-address
                       (sb-bsd-sockets:get-host-by-name (name instance)))))
                 (format NIL "~A.~A.~A.~A"
                         (aref hostent 0)
                         (aref hostent 1)
                         (aref hostent 2)
                         (aref hostent 3)))))
        ((not (slot-boundp instance 'name))
         (setf (slot-value instance 'name)
               (sb-bsd-sockets:host-ent-name
                (sb-bsd-sockets:get-host-by-address
                 (let ((arr (make-array '(4))))
                   (loop 
                      for i from 0 
                      for element in 
                        (mapcar #'read-from-string
                                (cl-ppcre:split #\. (address instance)))
                      do
                      (setf (aref arr i) element))
                   arr))))))
    (loop for monitor in (monitors instance)
	  do (post-config-fixup monitor))
    ))


(defgeneric initial-enqueue (object))
(defmethod initial-enqueue ((object equipment))
  (loop for mon in (monitors object)
	do (initial-enqueue mon)))
(defmethod initial-enqueue ((object monitor))
  (noctool-scheduler:schedule object (+ 1
					(get-universal-time)
					(random (interval object)))))

(defmethod update-alert ((thing disk-container))
  (setf (alert-level thing)
        (reduce #'max (disk-list thing)
                :key 'alert-level :initial-value 0)))

(defmethod update-alert ((thing process-container))
  (setf (alert-level thing)
        (reduce #'max (processes thing)
                :key 'alert-level :initial-value 0)))

(defmethod update-alert ((thing host))
  (setf (alert-level thing)
        (reduce #'max (monitors thing)
                :key 'alert-level :initial-value 0)))

(defmethod (setf alert-level) :AFTER (new (alert-level alert-level))
  (when (slot-exists-p alert-level 'parent)
    (if (parent alert-level)
        (update-alert (parent alert-level)))))

(defclass todo-item ()
  ((interval :reader interval :initarg :interval)
   (last-dequeued :accessor last-dequeued :initarg :last-dequeued)
   (last-scheduled :accessor last-scheduled :initarg :last-scheduled)
   (last-status :accessor last-status :initarg :last-status)
   (thing :reader thing :initarg :thing)
   (description :reader description :initarg :description)
   ))
