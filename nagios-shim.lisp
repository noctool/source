(in-package #:noctool)

(defclass nagios-monitor (monitor)
  ((args :accessor args :initarg :args :initform nil)
   (last-sent :accessor last-sent :initarg :last-sent :initform nil)
   (last-received :accessor last-received :initarg :last-received :initform nil)
   (port :reader port :initarg :port) 
   ))

(defvar *nagios-protocol-version* 2)
(defvar *nagios-query-type* 1)
(defvar *nagios-response-type* 2)
(defparameter *nagios-buffer-size* 1034) ;;; NB: fix this...
(defvar *nagios-nrpe-port* 5666)

(defgeneric nagios-app-name (object)
  )
(defmethod nagios-app-name ((o nagios-monitor))
  (declare (ignorable o))
  "_NRPE_CHECK")

(defmacro defnagios (class-name (app-name
				 &key (port *nagios-nrpe-port*)
				 )
		     &rest default-args)
  `(progn
    (initialize-crc32)
    (defclass ,class-name (nagios-monitor)
      ()
      (:default-initargs :port ,port :args ,default-args)
      )
    (defmethod nagios-app-name ((o ,class-name))
      (declare (ignorable o))
      ,(string app-name))))

(defun make-nagios-buffer ()
  (let ((rv (make-array *nagios-buffer-size*
			:element-type '(unsigned-byte 8)
			:initial-element 0)))
    (loop for ix from 0 below *nagios-buffer-size*
	  do (setf (aref rv ix) (random 256)))
    rv))

(defun nagios-endian-convert (endian &rest octets)
  (case endian
    (:big (loop for octet in octets
		for acc = octet then (+ (* 256 acc) octet)
		finally (return acc)))
    (:little (loop for octet in octets
		   for scale = 1 then (* 256 scale)
		   sum (* octet scale)))))

(defun nagios-octetify (integer endian octets)
  (let ((rv (make-array octets
			:element-type '(unsigned-byte 8)
			:initial-element 0)))
    (loop for octet from 0 below octets
	  do (setf (aref rv (if (eql endian :little)
				octet
				(- octets octet 1)))
		   (ldb (byte 8 (* 8 octet)) integer)))
    rv))
			 
(defun nagios-set-value (array integer offset octets endian)
  (let ((octet-array (nagios-octetify integer endian octets)))
    (loop for ix from offset
	  for new across octet-array
	  do (setf (aref array ix) new))))


(defun nagios-packet-version (packet &key (endian :little))
  (let ((octet1 (aref packet 0))
	(octet2 (aref packet 1)))
    (nagios-endian-convert endian octet1 octet2)))
(defun (setf nagios-packet-version) (new packet &key (endian :little))
  (nagios-set-value packet new 0 2 endian)
  new)

(defun nagios-packet-type (packet &key (endian :little))
  (let ((octet1 (aref packet 2))
	(octet2 (aref packet 3)))
    (nagios-endian-convert endian octet1 octet2)))
(defun (setf nagios-packet-type) (new packet &key (endian :little))
  (nagios-set-value packet new 2 2 endian)
  new)

(defun nagios-packet-crc32 (packet &key (endian :little))
  (let ((octet1 (aref packet 4))
	(octet2 (aref packet 5))
	(octet3 (aref packet 6))
	(octet4 (aref packet 7)))
    (nagios-endian-convert endian octet1 octet2 octet3 octet4)))
(defun (setf nagios-packet-crc32) (crc packet &key (endian :little))
  (nagios-set-value packet crc 4 4 endian)
  crc)

(defun nagios-packet-result (packet &key (endian :little))
  (let ((octet1 (aref packet 8))
	(octet2 (aref packet 9)))
    (nagios-endian-convert endian octet1 octet2)))
;;; Should not need a setter!

(defun nagios-packet-payload (packet)
  (make-array 1024 :element-type '(unsigned-byte 8) :displaced-to packet :displaced-index-offset 10))

;;; This should work, however... there should be reading back and stuff
(defmethod process ((mon nagios-monitor))
  (let ((request (octetify (format nil "~s ~{~s~^ ~}"
				   (nagios-app-name mon)
				   (args mon))))
	(send-buffer (make-nagios-buffer))
	(recv-buffer (make-nagios-buffer)))
    (setf (nagios-packet-version send-buffer) *nagios-protocol-version*)
    (setf (nagios-packet-type send-buffer) *nagios-query-type*)
    (setf (nagios-packet-crc32 send-buffer) 0)
    (let ((request-buffer (nagios-packet-payload send-buffer)))
      (loop for octet across request
	    for ix from 0
	    do (setf (aref request-buffer ix) octet)
	    finally (setf (aref request-buffer ix) 0))
      (setf (aref request-buffer 1023) 0))
    (setf (nagios-packet-crc32 send-buffer) (compute-crc32 send-buffer))
    (setf (last-sent mon) send-buffer)
    (let ((stream (socket-stream (socket-connect (address (equipment mon))
						 *nagios-nrpe-port*))))
      (when stream
	(unwind-protect
	     (progn
	       (write-sequence send-buffer stream)
	       (finish-output stream)
	       (unwind-protect (read-sequence recv-buffer stream))
	       (setf (last-received mon) recv-buffer)) 
	  (close stream))))
    ))

				
    
	