(in-package #:net.hexapodia.noctool)

(defmethod default-monitors ((kit equipment))
  (loop for class in (class-list-object kit)
	do (let ((defaults (gethash class *monitor-map* nil)))
	     (loop for new-mon in defaults
		   do (unless (member new-mon (monitors kit)
				      :test (lambda (class obj)
					      (typep obj class)))
			(make-monitor new-mon kit))))))
