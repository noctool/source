(in-package #:net.hexapodia.noctool-config)

(defvar *scrap-package-name* "NOCTOOL-SCRAP-CONFIG-PACKAGE")
(defvar *symbol-counter* 0)


(defvar *loaded-configurations* nil)
(defvar *loaded-objects* nil)
(defvar *macro-nesting* nil)
(defvar *config-object* nil)
(defvar *disk-container* nil)
(defvar *proc-container* nil)
(defvar *my-password* nil)
(defvar *my-name* nil)
(defvar *snmp-object* nil)

(defun bodge-package ()
  (when (find-package *scrap-package-name*)
    (delete-package *scrap-package-name*))

  (let ((package (make-package *scrap-package-name*
			       :use '(#:net.hexapodia.noctool-config #:net.hexapodia.noctool))))
    package))

(defun new-symbol ()
  (let ((sym (intern (format nil "TMPSYM~d" (incf *symbol-counter*))
		     (find-package *scrap-package-name*))))
    (push sym *loaded-configurations*)
    sym))

(defun get-config-symbol (name)
  (let ((scrap (find-package *scrap-package-name*)))
    (or (find-symbol (string name) scrap)
	(intern (string name) scrap))))

(defun translate-format (c-fmt)
  "Translate C format strings, we are primarily concerned with %d and %0nd and %%"
  (flet ((sub (str start end repl)
	   (format nil "~a~a~a" (subseq str 0 start) repl (subseq str end)))
	 (emit (n)
	   (loop for x from 1 to n
		 do (format t " "))
	   (format t "^~%")))
    (let ((rv (copy-seq c-fmt)))
      (loop for start = 0 then (1+ pos)
	    for pos = (position #\% rv :start start :test #'char=)
	    while pos
	    do (cond ((char= (char rv (1+ pos)) #\0)
		      (let ((fmt-end (position #\d rv :test #'char=)))
			(let ((width (parse-integer rv :start (1+ pos) :end fmt-end)))
			  (setf rv (sub rv pos (1+ fmt-end) (format nil "~~~d,'0d" width))))))
		     ((char= (char rv (1+ pos)) #\%)
		      (setf rv (sub rv pos (+ 2 pos) "%")))
		     ((char= (char rv (1+ pos)) #\d)
		      (setf rv (sub rv pos (+ 2 pos) "~d")))
		     (t (format t "Unknown format control~%~a~%" rv)
			(emit pos))))
      rv)))

(defmacro expand-config-stanza (&rest body)
  "Macro-expand a configuration snippet. Intended solely as a debug aid."
  `(progn
    (bodge-package)
    (loop for form in ',body
          collect (macroexpand-1 form))))

(defmacro defnested (name args nesting &body clauses)
  (let ((nesting-test (cond ((symbolp nesting) `(member ,nesting *macro-nesting*))
			    (t (cons 'cl:or (loop for sym in nesting
						  collect `(member ,sym *macro-nesting*)))))))
  `(defmacro ,name ,args
     (unless ,nesting-test
       (error ,(format nil "Macro ~a must be nested" name)))
     ,@clauses)))

(defun choose-password (this remote)
  (if this
      (decode-base64 this :result :octet)
    (if *my-password*
	*my-password*
      (decode-base64 remote :result :octet))))

(defmacro peer (name remote-passwd &optional this-passwd port)
  `(push (make-instance 'remote-node
	  :destination ,name
	  :remote-passwd ,(decode-base64 remote-passwd :result :octet)
	  :my-name (or *my-name* (error "No local hostname defined"))
	  :my-passwd ,(choose-password this-passwd
				       remote-passwd)
	  ,@(when port (list :dst-port port)))
    *loaded-objects*))

(defmacro local-password (string)
  `(setf *my-password* ,(decode-base64 string :result :octet)))

(defmacro local-hostname (string-des)
  `(setf *my-name* ,(string string-des)))

(defmacro machine (name type &rest specifics)
  (let ((*config-object* (new-symbol))
	(*macro-nesting* (cons :machine *macro-nesting*)))
    `(progn
       (defvar ,*config-object* (make-instance ',type
					       :name ,name))
       ,@(mapcar #'macroexpand specifics)
       (push ,*config-object* *loaded-objects*)
       )))

(defnested disks (&rest diskspec)
  :machine
  (let ((*macro-nesting* (cons :disk *macro-nesting*)))
    `(let ((*disk-container* (car (noctool::make-monitor 'noctool::disk-container ,*config-object*))))
       ,@(mapcar #'macroexpand diskspec))))

(defnested disk (path disk-percent &optional (inodes-percent 90))
  :disk
  `(let ((platter (make-instance 'noctool::disk-monitor
				 :device ,path
				 :equipment ,*config-object*
				 :disk-percent ,disk-percent
				 :inodes-percent ,inodes-percent
                                 :parent *disk-container*)))
     (noctool::add-graphs platter)
     (when (member ,path (noctool::ignore-list *disk-container*) :test #'equal)
       (setf (slot-value *disk-container* 'noctool::ignore-list)
             (remove ,path (noctool::ignore-list *disk-container*) 
                     :test #'equal)))
     (push platter (noctool::disk-list *disk-container*))))

(defnested disk-ignore (&rest fs-specs)
  :disk
  `(progn ,@(loop for spec in fs-specs
		  collect (list 'push spec (list 'ignore-list '*disk-container*)))))

(defnested procs (&rest procspec)
  :machine
  (let ((*macro-nesting* (cons :procs *macro-nesting*)))
    `(let ((*proc-container* (car (noctool::make-monitor 'noctool::process-container ,*config-object*))))
       ,@(mapcar #'macroexpand procspec))))

(defnested proc (name &key (min 1) (max 1) owner)
    :procs
  `(let ((proc (make-instance 'noctool::process-monitor
                              :name ,name 
                              :owner ,owner
                              :equipment ,*config-object*
                              :min ,min
                              :max ,max
                              :parent *proc-container*)))
     (format t "making proc: ~A~% " proc)
     (push proc (noctool::processes *proc-container*))))

(defnested snmp (&rest snmp-spec)
  :machine
  (let ((snmp-options nil)
	(snmp-monitors nil)
	(*macro-nesting* (cons :snmp *macro-nesting*)))
    (loop for (option . rest) in snmp-spec
	 do (case option
	      ((version public private) (push (cons option rest) snmp-options))
	      (t (push (cons option rest) snmp-monitors))))
  `(let ((*snmp-object* (car (noctool::make-monitor 'noctool::snmp-container ,*config-object*))))
     (loop for (option value) in ',snmp-options
	  do (case option
	       (version (setf (noctool::version *snmp-object*)
			      (cond ((member value
					     '(1 :v1 v1 "v1" "1" :test #'equalp))
				     :v1)
				    ((member value '(2 v2c :v2c "2" "v2c"))
				     :v2c)
				    ((member value '(3 v3 "3" "v3"))
				     :v3)
				    (t (error (format nil "Unknown SNMP version ~a" value))))))
	       (public (setf (noctool::public *snmp-object*) value))
	       (private (setf (noctool::private *snmp-object*) value))))
     ,@(mapcar #'macroexpand snmp-monitors))))
					      
(defnested interfaces (&rest interface-list)
  :snmp
  `(cond ((null ',interface-list) (setf (noctool::interfaces ,*snmp-object*) :all))
	 ((or (equal '(all) ',interface-list)
	      (equal '(:all) ',interface-list))
	  (setf (noctool::interfaces *snmp-object*) :all))
	 (t (setf (noctool::interfaces *snmp-object*)
		  (mapcar #'noctool::make-snmp-interface ',interface-list)))))


(defnested user (name &optional (passwd nil pw-provided))
  :machine
  `(progn
     (setf (slot-value ,*config-object* 'noctool::username) ,name)
     (when ,pw-provided
       (setf (slot-value ,*config-object* 'noctool::passwd) ,passwd))))

(defnested ip (address)
  :machine
  `(progn
     (setf (slot-value ,*config-object* 'noctool::address) ,address)))

(defnested ping (&rest args &key
		       (max-rtt 20) (max-fail 1) (ping-count 5)
		       (interval 60))
    :machine
  (list max-rtt max-fail interval ping-count)
  `(noctool::make-monitor 'ping-monitor ,*config-object* ,@args))

(defnested load-monitor (&optional (low-water 1.0) (high-water 5.0))
    :machine
  `(noctool::make-monitor 'load-monitor ,*config-object* :low-water ,low-water :high-water ,high-water))

(defmacro defmon (mon-class)
  (export (list mon-class))
  `(defnested ,mon-class (&rest options) :machine
     `(noctool::make-monitor ',',mon-class ,*config-object* ,@options)))

(defmacro cluster ((fmt low high &key (counter (gensym "CFGCNT")) (name nil) (c-fmt t)) form)
  (let ((format-string (if c-fmt
			   (translate-format fmt)
			   fmt)) 
	(name (or name
		  (get-config-symbol "NAME"))))
    `(let ((,counter 0))
      ,@(loop for n from low to high
	      for realname = (format nil format-string n)
	      collect `(setf ,counter ,n)
	      collect (substitute realname name form)))))

(defmacro with-format (bind-list &body body)
  (let ((let-bindings (loop for (var fmt-string . vars) in bind-list
			    collect `(,var (format nil ,fmt-string ,@vars)))))
    `(let ,let-bindings
      ,@body)))

(defun load (file)
  (let ((load-package (bodge-package)))
    (let ((*package* load-package)
	  (*read-eval* nil)
	  (*loaded-configurations* nil)
	  (*loaded-objects* nil)
	  )
      (cl:load file)

      (loop for val in *loaded-objects*
	    do (post-config-fixup val)
	    do (cond ((typep val (find-class 'noctool::equipment))
		      (noctool::default-monitors val)
		      (push val *equipment*))
		     ((typep val (find-class 'noctool::proxy))
		      (push val *proxies*))
		     ((typep val (find-class 'noctool::view))
		      (push val *views*))
		     ((typep val (find-class 'noctool::remote-node))
		      (setf (gethash (destination val) *peers*) val))
		     (t (format t "DEBUG: Object ~a matched no known case (this should not happen).~%" val))))
      )))
