(in-package #:cl-user)

(asdf:defsystem :noctool
  :author "Ingvar Mattsson / Jim Prewett"
  :license "GPL"
  :version "0.1"
  :depends-on (:usocket :cl-ppcre :ironclad :image :sb-posix :cl+ssl
			:snmp
			:cffi)
  :components ((:file "packages")
	       (:file "ssh-package")
	       (:file "ssh-cffi" :depends-on ("ssh-package"))
	       (:file "scheduler" :depends-on ("packages"))
	       (:file "network-globals" :depends-on ("packages"))
	       (:file "globals" :depends-on ("packages"))
	       (:file "generics" :depends-on ("packages" "globals"))
	       (:file "classes" :depends-on ("packages" "globals" "graphing" "graph-utils"))
	       (:file "methods" :depends-on ("packages" "classes" "generics"))
	       (:file "graphing" :depends-on ("packages" "network-globals"))
	       (:file "graph-utils" :depends-on ("packages" "graphing"))
	       (:file "graph-monitors" :depends-on ("packages"  "classes"))
	       (:file "config" :depends-on ("utils" "packages" "classes" "globals" "generics"))
	       (:file "utils" :depends-on ("packages" "scheduler" "classes"))
	       (:file "tests" :depends-on ("packages" "graph-utils" "globals" "classes" "utils" "scheduler"))
	       (:file "default-settings" :depends-on ("packages" "globals"))
	       (:file "network-utils" :depends-on ("packages" "network-globals"))
	       (:file "network" :depends-on ("scheduler" "packages" "network-utils" "network-globals"))
	       (:file "network-remote-calls" :depends-on ("packages" "network-globals" "network"))
	       (:file "todolist" :depends-on ("packages" "scheduler" "classes"))
	       (:file "nagios-shim" :depends-on ("packages" "utils" "classes"))
  ))
