(defun testcase (host &optional (count 5))
  (let ((args `("ping" "-c" ,(format nil "~d" count) ,host)))
    (let ((pty (sb-ext:process-pty
		(sb-ext:run-program "/usr/bin/env" args :wait nil :pty t))))
      (loop for line = (read-line pty nil)
	    do (format t "~a~%" line)))))

(testcase "127.0.0.1")

