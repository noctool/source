(in-package #:net.hexapodia.noctool)

(defvar *warning* 30 "Level where we consider things to be in Amber Alert")
(defvar *alerting* 70 "Level where we consider things to be broken/down/in a bad state.")

(defvar *views* nil "List of defined view styles")

(defvar *storage-path* (make-pathname :directory "/var/lib/noctool" :type "gdat"))

(defvar *monitor-map* (make-hash-table) "Mappings from class to default monitor types")

(defvar *graph* nil)
(defvar *equipment* nil)
(defvar *proxies* nil)
(defvar *peers* (make-hash-table :test #'equal))
(defvar *monitors* nil)

(defvar *noctool-package* (find-package '#:net.hexapodia.noctool))

(defun add-default-monitor (class monitor-class)
  (push monitor-class (gethash class *monitor-map* nil)))

(defmacro default-monitor (class monitor-class)
  `(add-default-monitor (find-class ',class) ',monitor-class))

(defun find-equipment-by-name (name)
  (loop for e in *equipment*
       when (equal (name e) name)
       do (return e)))
