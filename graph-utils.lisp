(in-package #:net.hexapodia.noctool-graphs)

(defvar *monitor-graph-map* (make-hash-table))
(defvar *class-compose-map* (make-hash-table :test #'equal))

(defun compose-graph-class (data display)
  (let ((cache (gethash (list data display) *class-compose-map*)))
    (if cache
	cache
        (let ((superclasses (list data display))
              (name (intern (format nil "GRAPH-COMPOSE+~a+~a"
                                    (symbol-name data)
                                    (symbol-name display))
                            (find-package :noctool-graphs))))
          (ensure-class name
                        :direct-superclasses superclasses)
          (setf (gethash superclasses *class-compose-map*) name)))))

(defclass graph-info ()
  ((slot :reader slot :initarg :slot)
   (data :reader data :initarg :data)
   (display :reader display :initarg :display)
   ))
 
(defmacro add-graph-info (monitor slot data display)
  `(push (make-instance 'graph-info
			:slot ',slot
			:data ',data
			:display ',display)
	 (gethash ',monitor *monitor-graph-map*)))


(defun add-graphs (monitor)
  (let ((class (class-name (class-of monitor))))
    (let ((graphs (gethash class *monitor-graph-map*)))
      (loop for graph-info in graphs
	    do (setf (slot-value monitor (slot graph-info))
		     (make-graph (compose-graph-class (data graph-info)
						      (display graph-info) )
				 :interval (interval monitor)))))))
