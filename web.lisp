(in-package :sw)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (require :cl-who)
  (require :symbolicweb)
  (use-package :cl-who)
  (use-package :symbolicweb))

(setf *SHOW-LISP-BACKTRACES-P* t)
(setf *SHOW-LISP-ERRORS-P* t)

;; swank
#-no-swank
(eval-when (:load-toplevel :execute)
  (progn
  (require :swank)
  (defparameter *swank-port* 4000)
  (defvar *swank-server* (swank:create-server :dont-close t :port *swank-port*))
  (setf swank:*use-dedicated-output-stream* NIL)))

;; with-gensyms is modified from Paul Graham On Lisp
(defmacro with-gensyms (symbols &body body)
  "Binds a whole list of variables to gensyms.  If the variables are lists, then the car is the symbol and the cadr is the string to use for the gensym."
  `(let ,(mapcar #'(lambda (symbol) (if (listp symbol)
                                        `(,(car symbol) (gensym ,(cadr symbol)))
                                        `(,symbol (gensym))))
                 symbols)
     ,@body))

(defmacro app-control-pane ()
  `(car (children-of *root*)))
(defmacro app-login-pane ()
  `(cadr (children-of *root*)))
(defmacro app-systems-pane ()
  `(caddr (children-of *root*)))
(defmacro app-info-pane ()
  `(cadddr (children-of *root*)))

(defwidget monitor (container)
  ((monitor :accessor monitor :initarg :monitor :initform (error "monitor must be supplied"))))

(defmethod initialize-instance :AFTER ((monitor monitor) &key)
  (add-to monitor 
          (mk-span (symbol-name (class-name (class-of (monitor monitor)))))))

(defvar *noc-host-hash* (make-hash-table))
 
(defwidget noc-host (container)
  ((noc-host :accessor noc-host :initarg :noc-host :initform (error "noc-host must be supplied"))))

(defmethod initialize-instance :AFTER ((noc-host noc-host) &key)
  (add-to noc-host 
          (mk-span (noctool::name (noc-host noc-host)))))

(defmacro defensure (name widget-form hash &body body)
  `(defmacro ,name (instance )
     (with-gensyms ((val "VAL") 
                    (foundp "FOUNDP"))
       `(multiple-value-bind (,val ,foundp)
            (gethash ,instance ,,hash)
          (if ,foundp
              ,val
              (progn
                (let* ((instance ,instance)
                       (widget ,',widget-form))
                  ,@',body
                  (setf (gethash ,instance ,,hash) widget))))))))

(defmacro mytoggle (thing on-expr off-expr &key (default :on))
  (declare (ignore thing))
  (let ((v (gensym)))
    `(let ((,v (eq ,default :on)))
       (iambda 
         (if (setf ,v (not ,v))
           ,on-expr
           ,off-expr)))))

(defmacro defhashes (&rest hashes)
  (cons 'progn
        (loop for hash in hashes collect
             (if (listp hash)
                 `(defvar ,(car hash) (make-hash-table :test ,(cadr hash)))
                 `(defvar ,hash (make-hash-table))))))

;; XXX do I need all of these damn hashes??? 
(defhashes *MONITOR-HASH* *LAST-UPDATED-HASH* *ALERT-LEVEL-HASH* *NEXT-RUN-HASH* *LAST-RTT-HASH* *LAST-FAILED-HASH* *LAST-OVER-RTT-HASH* *LAST-LOAD-HASH* *DISK-WIDGET-HASH* *PROC-WIDGET-HASH* *PROC-FOUND-HASH* *LOAD-1-HASH* *LOAD-5-HASH* *LOAD-10-HASH* (*METHOD-HASH* #'equal))

(defmacro reset-hashes (&rest hashes)
  (cons 'progn
        (loop for hash in hashes collect
             (if (listp hash)
                 `(setf ,(car hash) (make-hash-table :test ,(cadr hash)))
                 `(setf ,hash (make-hash-table))))))

(defmacro get-method-actions (method instance)
  (let ((i instance))
    `(gethash (list ,method ,i) *method-hash*)))

(defmacro add-method-actions (method instance action)
  `(setf (get-method-actions ',method ,instance)
         (append 
          (get-method-actions ',method ,instance)
          (list ,action))))

(defmacro method-after-method (method instance (&rest args))
  (let ((actions (gensym))
        (action (gensym)))
    `(defmethod ,method :after (,@args)
       (let ((,actions (get-method-actions ',method ,instance)))
         (mapcar
          (lambda (,action)
            (funcall ,action ,@(mapcan (lambda (x)
                                          (cond ((listp x)
                                                 (list (car x)))
                                                ((eql x '&optional)
                                                 ())
                                                (t (list x))))
                                       args)))
          ,actions)))))

(defmacro defaftermethod (method instance widget-form (&rest lambda-list) &body body)
  (let ((func (gensym))
        (actions (gensym))
        (action (gensym))
        (reduced-lambda-list (gensym)))
    `(let* ((instance ,instance)
            (widget ,widget-form)
            (,func #'(lambda (,@(mapcan (lambda (x)
                                          (cond ((listp x)
                                                 (list (car x)))
                                                ((eql x '&optional)
                                                 ())
                                                (t (list x))))
                                        lambda-list))
                      ,@body)))
       ;; set up the after method
       (method-after-method ,method ,instance ,lambda-list)
       ;; add this func to the list
       (add-method-actions ,method ,instance ,func))))

(defmacro ensure-load (name graph-slot hash)
  (with-gensyms ((graph "GRAPH") (span "SPAN"))
    `(defensure ,name
         (let* ((,graph (slot-value instance ',graph-slot))
                (,span (mk-span 
                        (format NIL "~2$" 
                                (aref (noctool-graphs::short ,graph) 
                                      (mod 
                                       (1- (noctool-graphs::short-ix ,graph))
                                       300)))
                        :css-class (alert-class (noctool::alert-level instance)))))
           ,span)
         ,hash
       (defaftermethod noctool::add-value instance widget
           ((graph (eql (slot-value instance ',graph-slot))) value)
         (setf (html-of widget)
               (format NIL
                       "~2$" 
                       (aref (noctool-graphs::short graph) 
                             (mod (1- (noctool-graphs::short-ix graph)) 300))))
         (setf (css-class-of widget)
               (alert-class (noctool::alert-level instance)))))))

(defun build-table (rows &key table-initargs)
  (let ((table (apply #'make-instance 'table table-initargs)))
    (loop for row in rows do (add-to table row))
    table))

(defun class-graphs (class)
  (mapcar #'noctool-graphs::slot
          (gethash (class-name (find-class class)) 
                   noctool-graphs::*monitor-graph-map*)))



(ensure-load ensure-load-1 noctool::graph-1 *load-1-hash*)
(ensure-load ensure-load-5 noctool::graph-5 *load-5-hash*)
(ensure-load ensure-load-10 noctool::graph-10 *load-10-hash*)

(defwidget load-widget (html-container)
  ((monitor :initform (error "monitor must be supplied")
            :initarg :monitor :accessor monitor)
   (load-1 :accessor load-1 :initform NIL)
   (load-5 :accessor load-5 :initform NIL)
   (load-10 :accessor load-10 :initform NIL)))

(defmethod initialize-instance :after ((widget load-widget) &key)
  (with-slots (load-1 load-5 load-10 monitor) widget
    (setf load-1  (ensure-load-1 monitor)        
          load-5  (ensure-load-5 monitor)
          load-10 (ensure-load-10 monitor))
    ))

(defmethod generate-html ((widget load-widget))
  (with-slots (load-1 load-5 load-10 monitor) widget
    (who
      (:div
       (str "1 Min: ")
       (:sw load-1)
       (str " 5 Min: ")
       (:sw load-5)
       (str " 10 Min: ")
       (:sw load-10)
       ))))

(defensure ensure-monitor-widget (make-instance 'monitor :monitor instance :css-class (alert-class (noctool::alert-level instance))) *monitor-hash*
  (let ((i instance))
    (defaftermethod (setf noctool::alert-level) instance widget (new (instance (eql instance)))
        (setf (css-class-of widget)
              (alert-class (noctool::alert-level instance))))))

(defmacro print-last-updated (instance)
  `(handler-case (sb-int:format-universal-time NIL (noctool::last-updated ,instance))
     (t () "Never")))

(defensure ensure-last-updated-widget
    (mk-span (print-last-updated instance)
             :css-class (alert-class (noctool::alert-level instance)))
    *last-updated-hash*
  (defaftermethod (setf noctool::last-updated) instance widget (new (instance (eql instance)))
    (setf (html-of widget)
          (sb-int:format-universal-time NIL new))
    (setf (css-class-of widget)
          (alert-class (noctool::alert-level instance)))))

(defensure ensure-alert-level
    (mk-span (format NIL "~A" (noctool::alert-level instance))
             :css-class (alert-class (noctool::alert-level instance)))
    *alert-level-hash*
  (defaftermethod (setf noctool::alert-level) instance widget (new (instance (eql instance)))
    (setf (html-of widget)
          (format NIL "~A" (noctool::alert-level instance)))
    (setf (css-class-of widget)
          (alert-class (noctool::alert-level instance)))))

(defensure ensure-next-run
    (let ((next-time (noctool-scheduler::time (noctool-scheduler::find-object instance noctool-scheduler::*default-scheduler*))))
      (mk-span (if next-time 
                   (sb-int:format-universal-time NIL next-time)
                   "None!") :css-class (alert-class (noctool::alert-level instance))))
    *next-run-hash*
  (defaftermethod noctool-scheduler::schedule instance widget ((object (eql instance)) time &optional scheduler)
    (let ((ts (noctool-scheduler::find-timeslot noctool-scheduler::*default-scheduler* time)))
      (when (and ts
                 (noctool-scheduler::find-object object ts))
        (setf (html-of widget)
              (sb-int:format-universal-time NIL time))
        (setf (css-class-of widget)
              (alert-class (noctool::alert-level instance)))
        ))))

(defensure ensure-last-rtt
    (mk-span 
     (let ((idx (noctool-graphs::short-ix (noctool::graph instance))))
       (format NIL "~A seconds" 
               (aref (noctool-graphs::short (noctool::graph instance)) 
                     (mod (1- idx) 300))))
     :css-class (alert-class (noctool::alert-level instance)))
    *last-rtt-hash*
  (defaftermethod noctool::add-value instance widget ((graph (eql (noctool::graph instance))) value)
    (setf (html-of widget)
          (format NIL "~A seconds" 
                  (aref (noctool-graphs::short 
                         graph)
                        (mod (1- (noctool-graphs::short-ix graph)) 300))))
    (setf (css-class-of widget)
          (alert-class (noctool::alert-level instance)))))

(defensure ensure-last-failed
    (mk-span 
     (format NIL "~A"
             (noctool::failed instance))
     :css-class (alert-class (noctool::alert-level instance)))
    *last-failed-hash*
  (defaftermethod (setf noctool::failed) instance widget (new (instance (eql instance)))
    (setf (html-of widget)
          (format NIL "~A" new))
    (setf (css-class-of widget)
          (alert-class (noctool::alert-level instance)))))

(defensure ensure-last-over-rtt
    (mk-span 
     (format NIL "~A"
             (noctool::over-rtt instance))
     :css-class (alert-class (noctool::alert-level instance)))
    *last-over-rtt-hash*
  (defaftermethod (setf noctool::over-rtt) instance widget (new (instance (eql instance)))
    (setf (html-of widget)
          (format NIL "~A" new))
    (setf (css-class-of widget)
          (alert-class (noctool::alert-level instance)))))

(defensure ensure-last-load 
    (make-instance 'load-widget :monitor instance)
  *last-load-hash*
  ())

(defensure ensure-noc-host-widget 
    (make-instance 'noc-host 
                   :noc-host instance 
                   :css-class (alert-class (noctool::alert-level instance))) 
    *noc-host-hash*
  (defaftermethod (setf noctool::alert-level)
      instance widget (new-val (host (eql instance)))
    (setf (css-class-of widget)
          (alert-class (noctool::alert-level instance)))))

(defun disk-info (disk)
  (list
   (escape-for-html (noctool::device disk))
   (- 100 (* 100 (handler-case 
                     (/ (noctool::disk-free disk) (noctool::disk-max disk))
                   (DIVISION-BY-ZERO () 1))))))

(defmacro disk-full (x)
  `(cadr (children-of ,x)))

(defensure ensure-disk-widget
    (let* ((info (disk-info instance))
           (widget (mk-table-row 
                    (list (mk-table-cell (mk-span (car info)))
                          (mk-table-cell (mk-span 
                                          (format NIL "~,3F" (cadr info))))))))
      (setf (css-class-of widget) (alert-class (noctool::alert-level instance)))
      widget)
    *disk-widget-hash*
  (defaftermethod (setf noctool::alert-level)
      instance widget (new-val (host (eql instance)))
    (setf (css-class-of widget)
          (alert-class (noctool::alert-level instance)))
    (setf (html-of (disk-full widget))
          (format NIL "~,3F" (cadr (disk-info instance))))))

(defmacro h/s (thing &key (hide "hide") (normal "normal"))
  `(mytoggle ,thing
           (setf (css-class-of ,thing) ,normal)
           (setf (css-class-of ,thing) ,hide) :default :off))

(defun h/s-all (state)
  (mapcar (lambda (child)
            (typecase child
              (eqp             
               (setf (css-class-of (cadr (children-of child)))
                     state))))
          (children-of (app-systems-pane))))

(defmethod alert-class ((i number))
  (cond ((>= i noctool::*alerting*)
           "alerting")
          ((>= i noctool::*warning*)
           "warning")
          (t
           "normal")))

(defwidget eqp (container)
  ((system-name :initform NIL :initarg :system-name :accessor system-name)
   (system :initform (error "system must be supplied") :initarg :system :accessor system)
   (namebox :initform (mk-container NIL)
            :accessor namebox)
   (monitors :initform NIL :initarg :monitors :accessor monitors)
   (monitorbox :initform (mk-container NIL)
               :accessor monitorbox)))



(defmethod initialize-instance :AFTER ((eqp eqp) &key)
  (with-slots (system namebox monitorbox) eqp
    (let ((namelink (mk-link (ensure-noc-host-widget system)))
          (expand-all (mk-link (mk-span "expand-all") 
                               :on-click 
                               (iambda
                                 (h/s-all "normal"))))
          (collapse-all (mk-link (mk-span "collapse-all")
                                 :on-click
                                 (iambda
                                   (h/s-all "hide"))))
          (info-show NIL))
      (add-to namebox namelink)
      (add-to eqp namebox)
      (setf (css-class-of monitorbox) "hide")
      (let ((h/s (h/s monitorbox)))
        (setf (on-click-of namelink)
              (lambda (&rest rest)
                (declare (ignore rest))
                (funcall h/s))))
      (add-to eqp monitorbox)
      (loop for mon in (noctool::monitors system) do 
           (let ((link (mk-link (ensure-monitor-widget mon) :href "#"))
                 (mon2 mon))
             (add-to monitorbox link)
               (setf (on-click-of link)
                     (iambda
                       ;; should we h/s the mon display box here?
                       (if (equal (visible *app*) mon2)
                           (progn
                             (remove-all (app-info-pane))
                             (setf (visible *app*) NIL))
                           (let ((info (display-monitor-info mon2)))
                             (remove-all (app-info-pane))
                             (add-to (app-info-pane) info)
                             (setf (visible *app*) mon2))))))))))

;; stuff to store the username/passwords for logins
(defclass user ()
  ((username :initarg :username :accessor username)
   (password :initarg :password :accessor password)))

(defvar *store-pathname* "store/")

(defvar *store* (cl-prevalence:make-prevalence-system *store-pathname*))

(defvar *login-table* (or (cl-prevalence:get-root-object *store* :login)
                          (setf (cl-prevalence:get-root-object *store* :login)
                                (make-hash-table :test #'equal))))

(defwidget login-widget (html-container)
  ((username :reader username :initform (mk-text-input))
   (password :reader password :initform (mk-text-input :password-p t))
   (login :reader button :initform (mk-button "login"))))

(defmethod generate-html ((login-widget login-widget))
  (with-slots (username password login) login-widget
    (who
     (:table :border 0
       (:tr (:td "Username: " (:sw username)))
       (:tr (:td "Password: " (:sw password)))
       (:tr (:td (:sw login)))))))

(defun add-user (username pass)
  (if (gethash username *login-table*)
      (error "user already exists")
      (prog1
          (setf (gethash username *login-table*)
                (make-instance 'user :username username :password pass))
        (cl-prevalence:snapshot *store*))))

(defun check-login (username pass)
  (let ((maybe-user (gethash username *login-table*)))
    (and maybe-user
         (equal (username maybe-user) username)
         (equal (password maybe-user) pass)
         maybe-user)))

(defmethod initialize-instance :after ((login-widget login-widget) &key)
  (with-slots (username password login) login-widget
    (setf 
     (on-click-of login)
     (iambda
       (let ((maybe-user (check-login (value-of username) (value-of password))))
         (format t "hello world~%")
         (if maybe-user
             (progn 
               (setf (logged-in-p *app*) t)
               (setf (user *app*)
                     maybe-user)
               (remove-all (app-login-pane))
               (setf (login *app*)
                     (mk-link (mk-span "Logout")))
               (add-to (app-login-pane) (login *app*))
               (setf (on-click-of (login *app*))
                     (iambda
                       (setf (logged-in-p *app*) NIL)
                       (setf (user *app*) NIL)
                       (setf (login *app*)
                             (mk-link (mk-span "Login")))
                       (reload))))
             (warn "bad u/p: ~A - ~A~%" 
                   (value-of username) (value-of password))))))))

(defmethod display-monitor-info (mon)
  (mk-container
   (list
    (mk-container 
     (list 
      (mk-span (noctool::name (noctool::equipment mon)))
      (mk-span " Alert Level: ")
      (ensure-alert-level (noctool::equipment mon))))
    (mk-span " Alert Level: ")
    (ensure-alert-level mon)
    (mk-span (who (:br)))
    (mk-span " Last Updated ")
    (ensure-last-updated-widget mon)
    (mk-span (who (:br)))
    (mk-span "Next Update: ")
    (ensure-next-run mon)
    (mk-span (who (:br))))))

(defmethod display-monitor-info ((mon noctool::disk-container))
  (mk-container
   (nconc
    (list
     (mk-container 
      (list 
       (mk-span (noctool::name (noctool::equipment mon)))
       (mk-span " Alert Level: ")
       (ensure-alert-level (noctool::equipment mon))))
     (mk-span (symbol-name (class-name (class-of mon))))
     (mk-span " Alert Level: ")
     (ensure-alert-level mon)
     (mk-span (who (:br)))
     (mk-span " Last Updated ")
     (ensure-last-updated-widget mon)
     (mk-span (who (:br)))
     (mk-span "Next Update: ")
     (ensure-next-run mon)
     (mk-span (who (:br))))
    (list 
     (build-table
      
      (cons
       (mk-table-row (list (mk-table-cell (mk-span "Disk"))
                           (mk-table-cell (mk-span "% FULL"))))
       (loop for disk in (noctool::disk-list mon)
          collect
            (ensure-disk-widget disk)))))
    (when (logged-in-p *app*)
      (let ((link (mk-link (mk-span "Update Monitor NOW!"))))
        (setf (on-click-of link)
              (iambda
                (when (logged-in-p *app*)
                (noctool::process mon))))
        (list link))))))

       (mk-table-row (list (mk-table-cell (mk-span "Owner"))
                           (mk-table-cell (mk-span "PID"))
                           (mk-table-cell (mk-span "PPID"))
                           (mk-table-cell (mk-span "Name"))
                           (mk-table-cell (mk-span "Args"))))

(defmacro with-slots-in-package ((&rest slots) package instance &body body)
  (let ((slts (loop for slot in slots collect
                   (list slot (intern (symbol-name slot) package)))))
    `(with-slots (,@slts) ,instance
       ,@body)))

(defensure ensure-process-found
    (mk-span (or (noctool::found-procs instance) ""))
    *proc-found-hash*
  (defaftermethod (setf noctool::found-procs)
      instance widget (new-val (inst (eql instance)))
    (setf (html-of widget) (format NIL "~A" new-val))))

(defensure ensure-process-widget
    (let ((widget 
           (mk-table-row 
            (list (mk-table-cell 
                   (mk-span (or (noctool::owner instance) "")))
                  (mk-table-cell 
                   (mk-span (format NIL "~A" (or (noctool::pid instance) ""))))
                  (mk-table-cell 
                   (mk-span (format NIL "~A" (or (noctool::ppid instance) ""))))
                  (mk-table-cell (mk-span (or (noctool::name instance) "")))
                  (mk-table-cell (mk-span (or (noctool::args instance) "")))
                  (mk-table-cell (mk-span (or (noctool::min-procs instance) "")))
                  (mk-table-cell (mk-span (or (noctool::max-procs instance) "")))
                  (mk-table-cell (ensure-process-found instance))))))
      (setf (css-class-of widget) (alert-class (noctool::alert-level instance)))
      widget)
    *proc-widget-hash*
  (defaftermethod (setf noctool::alert-level)
      instance widget (new-val (host (eql instance)))
    (setf (css-class-of widget)
          (alert-class (noctool::alert-level instance)))
    ))
 
(defmethod display-monitor-info ((mon noctool::process-container))
  (mk-container
   (nconc
    (list
     (mk-container 
      (list 
       (mk-span (noctool::name (noctool::equipment mon)))
       (mk-span " Alert Level: ")
       (ensure-alert-level (noctool::equipment mon))))
     (mk-span (symbol-name (class-name (class-of mon))))
     (mk-span " Alert Level: ")
     (ensure-alert-level mon)
     (mk-span (who (:br)))
     (mk-span " Last Updated ")
     (ensure-last-updated-widget mon)
     (mk-span (who (:br)))
     (mk-span "Next Update: ")
     (ensure-next-run mon)
     (mk-span (who (:br))))
    (list 
     (build-table
      (cons
       (mk-table-row (list (mk-table-cell (mk-span "Owner"))
                           (mk-table-cell (mk-span "PID"))
                           (mk-table-cell (mk-span "PPID"))
                           (mk-table-cell (mk-span "Name"))
                           (mk-table-cell (mk-span "Args"))
                           (mk-table-cell (mk-span "Min"))
                           (mk-table-cell (mk-span "Max"))
                           (mk-table-cell (mk-span "Found"))))
       (loop for proc in (noctool::processes mon)
          collect
            (ensure-process-widget proc))))))))

(defmethod display-monitor-info ((mon noctool::ping-monitor))
  (mk-container
   (list
    (mk-container 
     (list 
      (mk-span (noctool::name (noctool::equipment mon)))
      (mk-span " Alert Level: ")
      (ensure-alert-level (noctool::equipment mon))))
    (mk-span (symbol-name (class-name (class-of mon))))
    (mk-span " Alert Level: ")
    (ensure-alert-level mon)
    (mk-span (who (:br)))
    (mk-span " Last Updated ")
    (ensure-last-updated-widget mon)
    (mk-span (who (:br)))
    (mk-span "Next Update: ")
    (ensure-next-run mon)
    (mk-span (who (:br)))
    (mk-span "last rtt: ")
    (ensure-last-rtt mon)
    (mk-span " failed: ")
    (ensure-last-failed mon)
    (mk-span " over rtt: ")
    (ensure-last-over-rtt mon)
    (mk-span (who (:br)))
    )))

(defmethod display-monitor-info ((mon noctool::load-monitor))
  (mk-container
   (list
    (mk-container 
     (list 
      (mk-span (noctool::name (noctool::equipment mon)))
      (mk-span " Alert Level: ")
      (ensure-alert-level (noctool::equipment mon))))
    (mk-span (symbol-name (class-name (class-of mon))))
    (mk-span " Alert Level: ")
    (ensure-alert-level mon)
    (mk-span (who (:br)))
    (mk-span " Last Updated ")
    (ensure-last-updated-widget mon)
    (mk-span (who (:br)))
    (mk-span "Next Update: ")
    (ensure-next-run mon)
    (mk-span (who (:br)))
    (mk-span "1 Min: ")
    (ensure-load-1 mon)
    (mk-span " 5 Min: ")
    (ensure-load-5 mon)
    (mk-span " 10 Min: ")
    (ensure-load-10 mon)
    )))

(defapp noctool-app ()
  ((logged-in :accessor logged-in-p :initform NIL)
   (user :accessor user :initform NIL)
   (login :accessor login :initform (mk-link (mk-span "Login")))              
   (systems :accessor systems
            ;; :allocation :class
            :initform (loop for eqp in noctool::*equipment*
                         collect
                           (make-instance 'eqp :system eqp)))
   (visible :accessor visible :initform NIL))
   )

(set-uri 'noctool-app "/")

(defmethod render ((app noctool-app))
  (who
   (:html
    (:head
     (:title (str (format NIL "~A NOCTool" (sb-unix:unix-gethostname))))
     (str (js-sw-headers app))
     (:link :rel :stylesheet :type :text/css :href "/static/noctool.css"))
    (:body
     (str (sw-heading :title (string-downcase (princ-to-string (type-of app)))))
     (:div :id "sw-root")
     (:noscript "JavaScript needs to be enabled.")))))

(defvar *server-time* (mk-span "Server Time: Unknown"))
(defvar *server-time-thread* 
  (sb-thread:make-thread 
   #'(lambda ()
     (loop do (setf (html-of *server-time*)
                    (format NIL "Server time: ~A" (sb-int:format-universal-time NIL (get-universal-time)))) (sleep 5))) :name "some-thread"))

(defmethod render-viewport ((viewport viewport) (app noctool-app))
  (let ((systems-pane (mk-container NIL))
        (info-pane (mk-container NIL))
        (login-pane (mk-container (list (login app))))
        (control-pane (mk-container NIL)))
    (setf (on-click-of (login app))
          (iambda
            (remove-all (app-login-pane))
            (add-to (app-login-pane)
                    (make-instance 'login-widget))))
    (setf (css-class-of systems-pane) "systems")
    (setf (css-class-of info-pane)  "zoom")
    (with-slots (systems login) app
      (loop for eqp in noctool::*equipment*
           do
           (add-to systems-pane (make-instance 'eqp :system eqp)))
      (add-to systems-pane *server-time*)
      (add-to systems-pane (mk-div ""))
      (add-to control-pane (mk-link (mk-span "collapse all")
                                    :on-click (iambda (h/s-all "hide"))))
      (add-to control-pane (mk-span " "))
      (add-to control-pane (mk-link (mk-span "expand all")
                                    :on-click (iambda (h/s-all "normal"))))
      (add-to *root* control-pane)
      (add-to *root* login-pane)
      (add-to *root* systems-pane)
      (add-to *root* info-pane))))

(defvar *unwanted-monitors* '(noctool::cpu-monitor))

(defun schedule-all-monitors ()
  (loop for equipment in noctool::*equipment*
       do
       (loop for mon in 
            (remove-if 
             (lambda (x)
               (member (type-of x) *unwanted-monitors*))
             (noctool::monitors equipment))
          do
            (noctool::schedule mon (1+ (get-universal-time))))))

;; doesn't belong here!

(defun build-graph-classes ()
  (maphash 
   (lambda (k v) 
     (declare (ignore k))
     (loop for info in v do 
          (noctool-graphs::compose-graph-class 
           (noctool-graphs::data info)
           (noctool-graphs::display info))))
   noctool-graphs::*monitor-graph-map*))

(defun restore-saved-noctool ()
  (build-graph-classes)
  (let ((noctool::*dont-muck-with-instance* t))
    (defvar *store* (cl-prevalence:make-prevalence-system "/Users/dl/noctool/"))
    (unless noctool::*equipment*
      (setf noctool::*equipment* (cl-prevalence:get-root-object *store* :equipment)))
    (schedule-all-monitors)))
