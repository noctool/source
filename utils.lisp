(in-package #:net.hexapodia.noctool)

(defvar *base64* "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/")
(defvar *random-file* nil)
(defvar *crc32-table* nil)


(defmacro with-pty (ptyspec &body body)
  "Run a command with a PTY bound. If the ptyspec is (run-command ...), the
pty will be bound to the symbol PTY, otherwise the ptyspec is expected to be
a list (ptyvar command)"
  (let ((pty (if (eql (car ptyspec) 'run-command)
		 'pty
	       (car ptyspec)))
	(command (if (eql (car ptyspec) 'run-command)
		     ptyspec
		   (cadr ptyspec))))
    `(let ((,pty ,command))
       (unwind-protect
	   (progn ,@body)
	 (close ,pty)))))

(defun add-monitor (equipment monitor-type &rest args)
  (let ((mon (apply #'make-instance monitor-type :equipment equipment args)))
    (push mon (monitors equipment))
    (schedule mon (+ (get-universal-time) (interval mon)))
    (add-graphs mon)))

(defun make-telnet (host &optional (port 23))
  (socket-stream (socket-connect host port)))

(defun read-until-eof (stream)
  (with-output-to-string (s)
    (loop for line = (read-char stream nil stream)
	  until (eql line stream)
	  do (write-char line s))))

(defun run-command (command)
    #+sbcl
    (sb-ext:process-pty (sb-ext:run-program "/usr/bin/env" command :wait nil :pty t))
    #+cmucl
    (ext:process-pty (ext:run-program "/usr/bin/env" command :wait nil :pty t)))

(defun make-ssh (host &optional user port)
  "Make an ssh connection to HOST, using USER and PORT, as appropriate.
   Currently uses the host OS connection. For smooth function, use
   a passphraseless key to connect with."
  (let ((command `("ssh" ,@(when port (list "-p" (format nil "~d" port)))
		   ,(if user
			(format nil "~a@~a" user host)
		      host))))
    (let ((pty (run-command command)))
      (list pty pty))
    ))

(defun make-ssh-command (command host &optional user port)
  (let ((command `("ssh" ,@(when port (list "-p" (format nil "~d" port)))
		   ,(if user
			(format nil "~a@~a" user host)
		      host)
		   ,command)))
    (run-command command)))

(defun make-ssh-lib-command (command host &key (user "none") (port 22) password)
  (let ((session (noctool-ssh:ssh-connect host user :port port :password password)))
    (noctool-ssh::make-channel session :command command)
    (let ((string (noctool-ssh:ssh-read session)))
      (noctool-ssh::ssh-close session)
      (let ((stream (make-string-input-stream string)))
	(list stream stream)))))

(defun ping-helper (key string)
  (let ((start (search key string)))
    (when start
      (let ((end (position #\Space string :start start)))
	(when end
	  (let ((start (position #\= string :start start :end end)))
	    (read-from-string (subseq string (1+ start) end))))))))

(defun read-pty-line (pty &optional (eof-error-p t) eof-value)
  (if (not eof-error-p)
      (or (ignore-errors (read-line pty eof-error-p eof-value))
	  eof-value)
    (read-line pty)))

(defun make-ping (host &key (count 5) (interval 1))
  "Start a ping session to HOST, sending COUNT (default 5) packets.

   This function will need tailoring depending on the host OS.

   Return a vector with ms response times (using -1.0 as a placeholder
   for missed values)."

  (let ((args `("ping" "-c" ,(format nil "~d" count) 
                       "-i" ,(format nil "~d" interval)
                       ,host))
	(rv (make-array count :initial-element -1.0)))
    (with-pty (run-command args)
      (loop for line = (read-pty-line pty nil)
	    for n below (+ 4 count)
           with shift = 1
	    do (when (and line (<= 1 n count))
		 (let ((seq (ping-helper "icmp_seq" line))
		       (rtt (ping-helper "time" line)))
		   (when (and seq rtt)
                     (when (eql 0 seq)
                       (setf shift 0))
		     (setf (aref rv (- seq shift)) rtt))))))
    rv))

(defun decay-alert (old new)
  (max (- old 5)
       new
       0))

(defun split-line (line)
  (let ((line (string-trim '(#\Space #\Newline #\Return) line)))
    (loop for ch across line
	  for here from 0
	  with start = 0
	  if (and (char= ch #\space)
		  (< start here))
	  collect (prog1
		      (subseq line start here)
		    (setf start (position #\Space line
					  :start here :test #'char/=)))
	  into bag
	  finally (return (nreverse (cons (subseq line start) (nreverse bag))))
	  )))

(defgeneric match (spec string)
  )

(defmethod match ((spec string) (obj string))
  (search spec obj))

(defmethod match ((spec function) (obj string))
  (ppcre:scan spec obj))

(defgeneric get-ping-monitor (obj))

(defmethod get-ping-monitor ((eq equipment))
  (find 'ping-monitor (monitors eq) :test (lambda (a b)
					    (typep b a))))

(defmethod get-ping-monitor ((mon ping-monitor))
  mon)

(defmethod get-ping-monitor ((mon monitor))
  (get-ping-monitor (equipment mon)))

;; (defun host-pings (mon)
;;   (let ((ping-mon (get-ping-monitor mon)))
;;     (and (> (failed (get-ping-monitor mon)) 0)
;;          (< (alert-level (get-ping-monitor mon))
;;             *alerting*))))

(defun host-pings (mon)
  (let ((ping-mon (get-ping-monitor mon)))
    (or
     (< (alert-level (get-ping-monitor mon))
        *alerting*)
     (eql (failed ping-mon) 0))))

;;; Text encoding
(defun decode-base64 (str &key (result :latin1))
  "Uncode a BASE64 string. The RESULT keyword should be either :latin1 for LATIN-1 characters or :OCTET for an array of octets"
  (let ((len (length str))
	(adjust (count #\= str :test #'char=)))
    (let ((rvlen (- (/ (* len 6) 8) adjust)))
      (let ((rv (make-array rvlen :element-type (if (eql result :octet)
						    '(unsigned-byte 8)
						  'character))))
	(loop for spos from 0 below len by 4
	      for dpos from 0 by 3
	      for value = (loop with val = 0
				for offset from 0 to 3
				do (let ((ch (position (char str (+ spos offset)) *base64* :test 'char=)))
				     (setf val (+ (ash val 6)
						  (or ch 0))))
				finally (return val))
	      do (loop for offset from 0 to 2
		       do (let ((val (ldb (byte 8 (* 8 (- 2 offset))) value))
				(dest (+ dpos offset)))
			    (when (< dest rvlen)
			      (setf (aref rv dest)
				    (if (eql result :octet)
					val
				      (code-char val)))))))
	rv))))

(defun octetify (str)
  (coerce
   (loop for val across str
	 append (let ((n (etypecase val
			   (character (char-code val))
			   (integer val))))
		  (loop for pos downfrom (* 8 (truncate (floor (log (max 1 n) 2)) 8)) to 0 by 8
			collect (ldb (byte 8 pos) n))))
   '(vector (unsigned-byte 8))))

(defun encode-base-64-octet-vector (octet-vector)
  (let ((start octet-vector)
	(len (length octet-vector)))
    (labels ((get-val (base offset)
	       (let ((pos (+ base offset)))
		 (if (< pos len)
		     (aref start pos)
		     0))))
      (with-output-to-string (out)
	(loop for pos from 0 below (* 3 (ceiling len 3)) by 3
	      for val = (+ (* 256 256 (get-val pos 0))
			   (* 256 (get-val pos 1))
			   (get-val pos 2))
	      do (loop for n from 0 below 4
		       for offset downfrom 18 to 0 by 6
		       if (< (+ (truncate (* n 6) 8) pos) len)
		       do (write-char (char *base64* (ldb (byte 6 offset) val))
				      out)
		       else
		       do (write-char #\= out)))))))

(defun encode-base64 (str)
  (encode-base-64-octet-vector (octetify str)))

;;; Crypto helpers
(defun make-nonce (&optional (bits 160))
  "Generate a nonce, with roughly BITS random bits (specifically, at least 
that many bits, but it will be read as octets, so may be a bit more), then 
returned as a base64-encoded string."
  (let* ((octets (ceiling bits 8))
	 (data (make-array octets :element-type '(unsigned-byte 8))))
    #+linux
    (progn
      (unless *random-file*
	(setf *random-file* (open "/dev/urandom"
				  :element-type '(unsigned-byte 8))))
      (read-sequence data *random-file*))
    #-linux
    (loop for ix from 0 below octets
	  do (setf (aref data ix) (random 256)))

    (encode-base-64-octet-vector data)))
;;;
(defun get-peer (name)
  (gethash (string name) *peers*))

(defun get-class (name)
  (find-class (find-symbol (string name) *noctool-package*)))

(defun initialize-crc32 ()
  (unless *crc32-table*
    (let ((poly #xEDB88320))
      (setf *crc32-table* (make-array 256
				      :element-type '(unsigned-byte 32)
				      :initial-element 0))
      (loop for ix from 0 to 255
	    do (loop with crc = ix
		     for cnt from 8 downto 1
		     do (setf crc (if (oddp crc)
				      (logxor poly (ash crc -1))
				      (ash crc -1)))
		     finally (setf (aref *crc32-table* ix)
				   crc))))))

(defun compute-crc32 (octet-vector)
  "Computes the CRC32 of an octet vector" 
  (let ((crc #xFFFFFFFF))
    (loop for octet across octet-vector
	  for ix = (logand (logxor crc octet) #xFF)
	  do (setf crc (logxor (ash crc -8)
			       (aref *crc32-table* ix))))
    (logxor crc #xFFFFFFFF)))
