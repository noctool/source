(in-package :noctool-graphs)

(defvar *graph-colors* '((0 255 0) (255 0 0) (0 0 255) (255 128 0) (255 0 255) (0 63 63)))
(defvar *magnitudes* '(( -3 . "m") ( 3 . "k")
		       ( -6 . "u") ( 6 . "M")
		       ( -9 . "n") ( 9 . "G")
		       (-12 . "p") (12 . "T")))
(defvar *days* #("Mon" "Tue" "Wed" "Thu" "Fri" "Sat" "Sun"))

(defun graph-ignore (&rest args)
  (declare (ignore args))
  nil)

(defun nearest-multiplier (n)
  (if (zerop n) (values 0 0)
      (let ((pow (log n 10)))
	(multiple-value-bind (floor mod)
	    (floor pow 3)
	  (values (* 3 floor) (mod mod 3))))))

(defun date-format (time selector)
  (multiple-value-bind (second minute hour date month year day)
      (decode-universal-time time)
    (declare (ignore second year))
    (case selector
      (:short (format nil "~2,'0d:~2,'0d" hour minute))
      (:medium (format nil "~a" (aref *days* day)))
      (:long (format nil "~2,'0d-~2,'0d" month date)))))


(defun draw-grid (image max interval selector scale unit &key (alpha 1.0) (base 0) (when (get-universal-time)))
  (multiple-value-bind (multiplier offset)
      (nearest-multiplier max)
    (let ((si-mult (cdr (assoc (+ base multiplier) *magnitudes*))))
      (when si-mult
	(let ((unit-name (format nil "~a~a" si-mult unit))
	      (mult (expt 10 (+ multiplier (floor offset))))
	      (mant (mod offset 1)))
	  (image:rect image 25 10 325 110 nil 0 0 0 alpha)
	  (image:text image unit-name 1 1 0 0 0 alpha)
	  (let ((step (cond ((< mant 0.3) 0.25)
			    ((< mant 0.5) 0.5)
			    (t 1.0))))
	    (loop for n from 0 below 10 by step
		  do (let ((y-offset (round (* scale (* n mult)))))
		       (when (< y-offset 100)
			 (let ((y (- 110 y-offset))
			       (value (if (zerop (floor offset))
					  n
					(round
					 (* n (expt 10 (floor offset)))))))
			   (image:line image 25 y 325 y 0 0 0 alpha)
			   (image:text image 
				 (format nil "~d"
					 value)
				 0 (- y 4)
				 0 0 0 alpha)))))))))
    (multiple-value-bind (time-delta boundary)
	(case selector
	  (:short (values interval (* 8 3600)))
	  (:medium (values (* 12 interval) (* (round interval 300)
					      86400)))
	  (:long (values (* 12 12 interval) (* (round interval 300)
					       (* 14 86400)))))
      (loop for x-offset from 0 below 300
	    for time from (- when (* 300 time-delta)) by time-delta
	    do (when (/= (floor time boundary)
			 (floor (+ time time-delta) boundary))
		 (let ((time (date-format (* boundary (round time boundary))
					  selector)))
		   (let ((width (image:text image time 0 0 0 0 0 0.0)))
		     (image:line image (+ x-offset 25) 10 (+ x-offset 25) 110
			   0 0 0 alpha)
		     (image:text image time
			   (- (+ x-offset 25) (truncate width 2)) 113
			   0 0 0 alpha))))))))


(defmethod show ((graph noctool::ping-monitor) sink format &key (selector :short) (background '(240 240 240))
                 scale &allow-other-keys)
  (let ((image (image:make-image 350 140)))
    (apply #'image:rect image 0 0 349 139 t background)
    (multiple-value-bind (percentile max scale)
	(show (noctool::graph graph) image format :selector selector :scale scale)
      (graph-ignore percentile max scale)
      )
    image))

(defmethod show ((graph noctool::disk-container) sink format &key (selector :short) scale &allow-other-keys)
  (graph-ignore scale)
  (let ((lines (length (noctool::disk-list graph)))
	(disks (sort (copy-list (noctool::disk-list graph)) #'> :key #'noctool::disk-max)))
    (let ((image (image:make-image 350 (+ 130 (* 10 lines)))))
      (image:rect image 0 0 349 (1- (image::height image)) t 192 192 192)
      (multiple-value-bind (percentile max scale)
	  (show (noctool::disk-graph (car disks)) image nil
		:color '(0 0 0 0.0)
		:base-x 25 :base-y 110 :height 100 :selector selector)
	(graph-ignore percentile)
	(draw-grid image max (interval graph) selector scale "b" :base 3 :when (noctool:last-updated graph))
	(loop for disk in disks
	      for color in *graph-colors*
	      for text-offset = 125 then (+ text-offset 10)
	      for style = :line then :plot
	      do (let ((graph (noctool::disk-graph disk))
		       (text (format nil "~a [ ~a ]"
				     (noctool::mountpoint disk)
				     (noctool::device disk))))
		   (show graph image nil :color color :scale scale
			 :height 100 :base-y 110 :base-x 25
			 :selector selector :style style)
		   (image:text image text 25 text-offset 0 0 0)
		   (image:rect image 15 text-offset 20 (+ 5 text-offset)
			 t (nth 0 color) (nth 1 color)(nth 2 color)))))
      image)))

(defmethod show ((graph noctool::disk-container) sink format &key (selector :short) (background '(192 192 192)) scale &allow-other-keys)
  (graph-ignore scale)
  (let ((lines (length (noctool::disk-list graph)))
	(disks (sort (copy-list (noctool::disk-list graph)) #'> :key #'noctool::disk-max)))
    (let ((image (image:make-image 350 (+ 130 (* 10 lines)))))
      (apply #'image:rect image 0 0 349 (1- (image::height image)) t background)
      (multiple-value-bind (percentile max scale)
	  (show (noctool::disk-graph (car disks)) image nil
		:color '(0 0 0 0.0) :base-x 25 :base-y 110 :height 100
		:selector selector)
	(graph-ignore percentile)
	(draw-grid image max (interval graph) selector scale "b" :base 3 :when (noctool:last-updated graph))
	(loop for disk in disks
	      for color in *graph-colors*
	      for text-offset = 125 then (+ text-offset 10)
	      for style = :line then :plot
	      do (let ((graph (noctool::disk-graph disk))
		       (text (format nil "~a [ ~a ]" (noctool::mountpoint disk) (noctool::device disk))))
		   (show graph image nil :color color :scale scale :height 100 :base-y 110 :base-x 25 :selector selector :style style)
		   (image:text image text 25 text-offset 0 0 0)
		   (image:rect image 15 text-offset 20 (+ 5 text-offset)
			 t (nth 0 color) (nth 1 color)(nth 2 color)))))
      image)))

(defmethod show ((graph noctool::snmp-interface) sink format &key (selector :short) (measure :octets) (background '(192 192 192)) &allow-other-keys)
  (flet ((measure1 () (case measure
			(:octets #'noctool::in-octets)
			(:discards #'noctool::in-discards)
			(:errors #'noctool::in-errors)))
	 (measure2 () (case measure
			(:octets #'noctool::out-octets)
			(:discards #'noctool::out-discards)
			(:errors #'noctool::out-errors))))
    (let ((unit (case measure
		  (:octets "b/s")
		  (:discards "discards")
		  (:errors  "errors")))
	  (image (image:make-image 350 140))
	  (interval 1))
      (multiple-value-bind (in-percentile in-max in-scale)
	  (show (funcall (measure1) graph) image nil :height 100 :base-y 110 :base-x 25)
	(multiple-value-bind (out-percentile out-max out-scale)
	    (show (funcall (measure2) graph) image nil :height 100 :base-y 110 :base-x 25)
	  (apply #'image:rect image 0 0 349 139 t background)
	  (let* (
;;		(scale (/ (if (>= in-max out-max) in-scale out-scale) interval))
		(max (max in-max out-max))
		(scale (/ 100 max))
;;		(min (min in-max out-max))
;;		(max (/ (max in-max out-max) interval))
;;		(min (/ (min in-max out-max) interval))
		(in-color (car *graph-colors*))
		(out-color (caddr *graph-colors*)))
	    
	    (format t "DEBUG: unit is ~s~%in ~f out ~f ~f~%in ~f out ~f ~f~%" unit in-max out-max max in-scale out-scale scale)

	    (draw-grid image (* max 8) (interval graph) selector (/ scale 8) unit)

	    (show (funcall (measure1) graph) image nil :style :line :scale scale :color in-color :base-x 25 :height 100 :base-y 110 :selector selector)
	    (show (funcall (measure2) graph) image nil :style :plot :scale scale :color out-color :base-x 25 :height 100 :base-y 110 :selector selector)

	    (let ((w1 (image:text image (noctool::name graph) 5 130 0 0 0)))
	      (let ((w2 (apply #'image:text image (format nil "~a in" unit) (+ w1 10) 130 in-color)))
		(apply #'image:text image (format nil "~a out" unit) (+ w2 10) 130 out-color)))

	    (draw-grid image (* max 8) (interval graph) selector (/ scale 8) unit :alpha 0.2)
	    
	    image))))))
      
(defmethod show ((graph noctool::snmp-container) sink format &rest keys &key (selector :short) (measure :octets) &allow-other-keys)
  (let ((keys (loop for (key val . rest) on keys by #'cddr
		   if (not (member key '(:selector :measure)))
		   append (list key val))))
    (mapcar (lambda (interface) (apply #'show interface sink format :selector selector :measure measure keys)) (noctool::interfaces graph))))

(defmethod show :around ((graph noctool::monitor) (sink string) format &key (selector :short) scale &allow-other-keys)
  (graph-ignore selector scale)
  (let ((image (call-next-method)))
    (image:export-image image sink)))
