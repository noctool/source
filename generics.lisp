(in-package #:net.hexapodia.noctool)


(defun make-monitor (type kit &rest options)
  (let ((monitor (apply 'make-instance type :equipment kit :parent kit options)))
    (push monitor (monitors kit))))

(defun class-list-class (class)
  (unless (eql class (find-class 'equipment))
    (let ((direct-supers (copy-list (class-direct-superclasses class))))
      (append direct-supers
	      (mapcan 'class-list-class direct-supers)))))

(defun class-list-object (obj)
  (class-list-class (class-of obj)))

(defgeneric default-monitors (kit))

