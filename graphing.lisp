(in-package #:net.hexapodia.noctool-graphs)

(defvar *graph-map* '((:gauge . gauge-graph) (:default . meter-graph)
		      (:meter . meter-graph) (:gauge-avg . avg-graph)
		      (:gauge-max . max-graph)))
;;; Graph storage classes
(defclass base-graph (noctool::id-object)
  ((short :reader short :initarg :short)
   (medium :reader medium :initarg :medium)
   (long :reader long :initarg :long)
   (short-ix :accessor short-ix :initarg :short-ix)
   (medium-ix :accessor medium-ix :initarg :medium-ix)
   (long-ix :accessor long-ix :initarg :long-ix)
   (interval :reader interval :initarg :interval)
   (proxies :accessor proxies :initform nil)
   (last-updated :accessor last-updated :initform NIL))
  (:default-initargs :short-ix 0 :medium-ix 0 :long-ix 0 :id (gentemp "GRAPH-" (find-package :noctool-symbols))))

(defclass gauge-graph (base-graph)
  ())
(defclass meter-graph (base-graph)
  ())
(defclass avg-graph (gauge-graph)
  ())
(defclass max-graph (gauge-graph)
  ())

;;; Serialisation aid
(defgeneric graph-type (graph))
(defmethod graph-type ((graph gauge-graph))
  (declare (ignore graph))
  "gauge-graph")
(defmethod graph-type ((graph meter-graph))
  (declare (ignore graph))
  "meter-graph")
(defmethod graph-type ((graph avg-graph))
  (declare (ignore graph))
  "avg-graph")
(defmethod graph-type ((graph max-graph))
  (declare (ignore graph))
  "max-graph")

(defun serialize-data (graph)
  (let ((short-ix (short-ix graph))
	(short (short graph))
	(medium-ix (medium-ix graph))
	(medium (medium graph))
	(long-ix (long-ix graph))
	(long (long graph)))
    (list
     (append (loop for ix from short-ix below 300
		   collect (aref short ix))
	     (loop for ix from 0 below short-ix
		   collect (aref short ix)))
     (append (loop for ix from medium-ix below 300
		   collect (aref medium ix))
	     (loop for ix from 0 below medium-ix
		   collect (aref medium ix)))
     (append (loop for ix from long-ix below 300
		   collect (aref long ix))
	     (loop for ix from 0 below long-ix
		   collect (aref long ix))))))

;;; Graph display classes
(defclass graph-display-base ()
  ((multiplier :reader multiplier :initarg :multiplier)
   ))

(defclass fixed-graph-display (graph-display-base)
  ((max-value :reader max-value :initarg :max-value)
   ))

(defclass percentile-graph-display (graph-display-base)
  ((percentile :reader percentile :initarg :percentile)
   )
  (:default-initargs :percentile 90))

(defgeneric add-value (graph value))
(defgeneric add-medium (graph))
(defgeneric add-long (graph))
(defgeneric show (graph sink format &key selector &allow-other-keys))
(defgeneric extract-display-data (graph selector))
(defgeneric store (graph &optional filename))

(defmethod store ((graph base-graph) &optional filename)
  (with-open-file (stream filename
			  :direction :output
			  :if-exists :supersede :if-does-not-exist :create)
    (format stream "(setf (slot-value *graph* 'short) ~a)~%~%" (short graph))
    (format stream "(setf (slot-value *graph* 'medium) ~a)~%~%" (medium graph))
    (format stream "(setf (slot-value *graph* 'long) ~a)~%~%" (long graph))
    (format stream "(setf (short-ix *graph*) ~d)~%" (short-ix graph))
    (format stream "(setf (medium-ix *graph*) ~d)~%" (medium-ix graph))
    (format stream "(setf (long-ix *graph*) ~d)~%" (long-ix graph)))
  filename)


(defun make-graph (type &rest args)
   (let ((short (make-array 300 :initial-element 0))
	 (medium (make-array 300 :initial-element 0))
	 (long (make-array 300 :initial-element 0))
	 (graph-type (or (cdr (assoc type *graph-map*)) type)))
     (if type
	 (apply #'make-instance graph-type :short short :medium medium :long long args)
       (error "Unknown graph type, ~a" type))))
    
;;; Data input

(defmacro bump-ix (graph ix)
  `(setf (,ix ,graph) (mod (1+ (,ix ,graph)) 300)))

(defmethod add-value (graph value)
  (when (zerop (mod (short-ix graph) 12))
    (add-medium graph))
  (setf (aref (short graph) (short-ix graph)) value)
  (bump-ix graph short-ix)
  (when (proxies graph)
    (loop for proxy in (proxies graph)
	  do (noctool-network:graph-update proxy (noctool:id graph) value))))

(defmethod add-value :after (graph value)
  (setf (last-updated graph) (get-universal-time)))

(defmethod add-medium ((graph meter-graph))
  (when (zerop (mod (medium-ix graph) 12))
    (add-long graph))
  (setf (aref (medium graph) (medium-ix graph))
	(aref (short graph) (short-ix graph)))
  (bump-ix graph medium-ix))

(defmethod add-medium ((graph gauge-graph))
  (when (zerop (mod (medium-ix graph) 12))
    (add-long graph))
  
  (let ((short-ix (short-ix graph))
	(medium-ix (medium-ix graph)))
    (let ((data (sort (subseq (short graph) short-ix (+ short-ix 11)) #'<)))
      (let ((value (/ (+ (aref data 5) (aref data 6)) 2)))
	(setf (aref (medium graph) medium-ix) value))))
  (bump-ix graph medium-ix))

(defmethod add-medium ((graph avg-graph))
  (when (zerop (mod (medium-ix graph) 12))
    (add-long graph))
  
  (let ((short-ix (short-ix graph))
	(medium-ix (medium-ix graph)))
    (let ((data (sort (subseq (short graph) short-ix (+ short-ix 11)) #'<)))
      (let ((value (/ (reduce #'+ data) 12)))
	(setf (aref (medium graph) medium-ix) value))))
  (bump-ix graph medium-ix))

(defmethod add-medium ((graph max-graph))
  (when (zerop (mod (medium-ix graph) 12))
    (add-long graph))
  
  (let ((short-ix (short-ix graph))
	(medium-ix (medium-ix graph)))
    (let ((data (sort (subseq (short graph) short-ix (+ short-ix 11)) #'<)))
      (let ((value (reduce #'max data)))
	(setf (aref (medium graph) medium-ix) value))))
  (bump-ix graph medium-ix))



(defmethod add-long ((graph meter-graph))
  (setf (aref (long graph) (long-ix graph))
	(aref (medium graph) (medium-ix graph)))
  (bump-ix graph long-ix))

(defmethod add-long ((graph gauge-graph))
  (let ((medium-ix (medium-ix graph))
	(long-ix (long-ix graph)))
    (let ((data (sort (subseq (medium graph) medium-ix (+ medium-ix 11)) #'<)))
      (let ((value (/ (+ (aref data 5) (aref data 6)) 2)))
	(setf (aref (long graph) long-ix) value))))
  (bump-ix graph long-ix))

(defmethod add-long ((graph avg-graph))
  (let ((medium-ix (medium-ix graph))
	(long-ix (long-ix graph)))
    (let ((data (sort (subseq (medium graph) medium-ix (+ medium-ix 11)) #'<)))
      (let ((value (/ (reduce #'+ data) 12)))
	(setf (aref (long graph) long-ix) value))))
  (bump-ix graph long-ix))

(defmethod add-long ((graph max-graph)) 
  (let ((medium-ix (medium-ix graph))
	(long-ix (long-ix graph)))
    (let ((data (sort (subseq (medium graph) medium-ix (+ medium-ix 11)) #'<)))
      (let ((value (reduce #'max data)))
	(setf (aref (long graph) long-ix) value))))
  (bump-ix graph long-ix))

;;; Data display
(defmethod extract-display-data ((graph meter-graph) selector)
  (let ((rv (make-array 300)))
    (multiple-value-bind (array
			  ix
			  interval)
	(case selector
	  (:short  (values (short graph)
			   (short-ix graph)
			   (interval graph)))
	  (:medium (values (medium graph)
			   (medium-ix graph)
			   (* 12 (interval graph))))
	  (:long   (values (long graph)
			   (long-ix graph)
			   (* 12 12 (interval graph))))
	  (t (error "Unknown graph time-period selector ~a" selector)))

      (flet ((extract (ix1 ix0)
		     (let ((v1 (aref array ix1))
			   (v0 (aref array ix0)))
		       (if (>= v1 v0)
			   (- v1 v0)
			   v1))))
      
      (loop for source from ix below 300
	    for dest from 0
	    do (setf (aref rv dest) (/ (- (extract (mod (1+ source) 300)
						   source))
				       interval)))
      (loop for source from 0 below ix
	    for dest from (- 300 ix)
	    do (setf (aref rv dest) (/ (- (extract (mod (1+ source) 300)
						   source))
				       interval))))
      rv)))

(defmethod extract-display-data ((graph gauge-graph) selector)
  (let ((array (case selector
		 (:short (short graph))
		 (:medium (medium graph))
		 (:long (long graph))
		 (t (error "Unknown graph time-period selector ~a" selector))))
	(ix (case selector
	      (:short (short-ix graph))
	      (:medium (medium-ix graph))
	      (:long (long-ix graph))))
	(rv (make-array 300)))
    (loop for source from ix below 300
	  for dest from 0
	  do (setf (aref rv dest) (aref array source)))
    (loop for source from 0 below ix
	  for dest from (- 300 ix)
	  do (setf (aref rv dest) (aref array source)))
    rv))

(defun plot-data (image x base-y offset style color)
  (let ((y (- base-y offset)))
    (case style
      (:line (apply 'image:line image x base-y x y color))
      (:plot (apply 'image:plot image x y color)))))

(defmethod show ((graph fixed-graph-display) sink format
		 &key (selector :short) (color '(0 255 0)) (base-x 0)
		 (base-y (1- (image::height sink))) (height (image::height sink))
		 (style :plot))
  (let ((data (extract-display-data graph selector)))
    (loop for ix from 0 below 300
	  do (setf (aref data ix)
		   (if (< (aref data ix) 0)
		       0
		     (* (multiplier graph) (aref data ix)))))
    (loop for n from 0
	  for value across data
	  do (plot-data sink
			(+ base-x n) base-y
			(round (* value height) (max-value graph))
			style color))
  ))

(defmethod show ((graph percentile-graph-display) sink format
		 &key (selector :short) (color '(0 255 0)) (base-x 0) scale
		 (base-y (1- (image::height sink))) (height (image::height sink))
		 (style :plot))
  (let ((data (extract-display-data graph selector)))
    (let ((tmpdata (sort (copy-seq data) #'<)))
      (let ((percentile (aref tmpdata (round (* (length tmpdata) (/ (percentile graph) 100))))))
	(let ((scale (or scale (if (zerop percentile)
				   (/ height (let ((max (reduce #'max tmpdata)))
                                               (if (eql 0 max) 1 max)))
				 (/ height percentile)))))
	  (loop for n from 0
		for value across data
		do (if (>= value 0)
                       (plot-data sink
                                  (+ base-x n) base-y (max 0 (round (* value scale)))
                                  style color)))
	  (values percentile (reduce #'max tmpdata) scale))))))

