(in-package #:net.hexapodia.noctool-scheduler)

(defvar *default-scheduler* nil)
(defvar *network-updates-needed* nil)

(defvar *total-processes* 
  #+darwin 8
  #-darwin 32)

(defvar *process-semaphore* (sb-thread:make-semaphore :name "Simultaneous processes" :count *total-processes*))

(defmacro with-semaphore (semaphore &body body)
  `(progn
    (sb-thread:wait-on-semaphore ,semaphore)
    (unwind-protect
	 (progn ,@body)
      (sb-thread:signal-semaphore ,semaphore))))

(defmacro noc-thread (&body body)
  `(with-semaphore *process-semaphore*
    ,@body))

(defclass event ()
  ((time :reader time :initarg :time)
   (object :reader object :initarg :object)))

(defclass timeslot ()
  ((time :reader time :initarg :time)
   (events :accessor events :initarg :events :initform nil)
   (next :accessor next :initarg :next :initform nil)
   (prev :accessor prev :initarg :prev :initform nil)
   (lock :accessor lock :initform (sb-thread:make-mutex :name "scheduler lock"))
   ))

(defclass scheduler ()
  ((first-timeslot :accessor first-timeslot :initarg :first-timeslot)
   (last-timeslot :accessor last-timeslot :initarg :last-timeslot)
   (lock :accessor lock :initform (sb-thread:make-mutex :name "scheduler lock"))
   )
  (:default-initargs :first-timeslot nil :last-timeslot nil))

(defgeneric find-object (object store))

(defmethod find-object (object (event event))
  (equal object (object event)))

(defmethod find-object (object (timeslot timeslot))
  (loop for event in (events timeslot)
       when (find-object object event)
       do (return t)))

(defmethod find-object (object (scheduler scheduler))
  (loop with timeslot = (first-timeslot scheduler)
       when (find-object object timeslot)
       do (return timeslot)
       when t
       do (if (equal timeslot (last-timeslot scheduler))
              (return NIL)
              (setf timeslot (next timeslot)))))

(defmethod time ((foo null))
  foo)
(defmethod first-timeslot ((foo null))
  foo)

(defmethod last-timeslot ((foo null))
  foo)
(defmethod prev ((foo null))
  foo)
(defmethod next ((foo null))
  foo)

;;; XXX assumes a slot named "lock" in the object :P
;;; D'OH!
(defmacro with-object-lock (scheduler &body body)
  (let ((lock (gensym)))
    `(let ((,lock (lock ,scheduler)))
       (sb-thread:with-mutex (,lock)
         ,@body))))

(defun find-timeslot (scheduler time)
    (cond ((null (first-timeslot scheduler))
	 (let ((new (make-instance 'timeslot :time time)))
	   (setf (first-timeslot scheduler) new)
	   (setf (last-timeslot scheduler) new)
	   new))
	  ((< time (time (first-timeslot scheduler)))
	   (let ((new (make-instance 'timeslot :time time)))
	     (setf (next new) (first-timeslot scheduler))
	     (setf (prev (first-timeslot scheduler)) new)
	     (setf (first-timeslot scheduler) new)
	     new))
	  ((> time (time (last-timeslot scheduler)))
	   (let ((new (make-instance 'timeslot :time time)))
	     (setf (prev new) (last-timeslot scheduler))
	     (setf (next (last-timeslot scheduler)) new)
	     (setf (last-timeslot scheduler) new)
	     new))
	  ((= time (time (last-timeslot scheduler)))
	   (last-timeslot scheduler))
	  ((= time (time (first-timeslot scheduler)))
	   (first-timeslot scheduler))
	  (t (let ((fdiff (- time (time (first-timeslot scheduler))))
		   (ldiff (- time (time (last-timeslot scheduler)))))
	       (cond ((< ldiff fdiff)
		      (loop for here = (last-timeslot scheduler) then (prev here)
			    for next = (prev (last-timeslot scheduler)) then (prev next)
			    do (cond ((= time (time here))
				      (return here))
				     ((< (time next) time (time here))
				      (let ((new (make-instance
						  'timeslot
						  :time time
						  :next here
						  :prev next)))
					(setf (next next) new)
					(setf (prev here) new)
					(return new))))))
		     (t
		      (loop for here = (first-timeslot scheduler) then (next here)
			    for next = (next (last-timeslot scheduler)) then (next  next)
			    do (cond ((= time (time here))
				      (return here))
				     ((< (time next) time (time here))
				      (let ((new (make-instance
						  'timeslot
						  :time time
						  :prev here
						  :next next)))
					(setf (next here) new)
					(setf (prev next) new)
					(return new)))))))))))

(defgeneric add-event (event store))
(defgeneric process (thing))

(defmethod add-event ((event event) (store scheduler))
  (with-object-lock store
    (let* ((time (time event))
           (object (object event))
           (slot (find-timeslot store time))
           (found (find-object object store)))
      (if found
          (progn (warn "not scheduling object: ~A as it is already scheduled at ~A!~%" object (time found)))
          (add-event event slot)))))

(defmethod add-event ((event event) (store timeslot))
  (with-object-lock store
    (when (= (time event) (time store))
      (push event (events store)))))

(defun remove-timeslot (timeslot)
  (when timeslot
    (with-object-lock timeslot
      (progn
        (setf (prev (next timeslot)) (prev timeslot))
        (setf (next (prev timeslot)) (next timeslot))
        (setf (prev timeslot) nil)
        (setf (next timeslot) nil)))))

(defun next-timeslot (&optional (scheduler *default-scheduler*))
  (with-object-lock (if scheduler 
                        scheduler     
                        (or *default-scheduler* 
                            (setf *default-scheduler* 
                                  (make-instance 'scheduler))))
    (prog1
        (first-timeslot scheduler)
      (setf (first-timeslot scheduler) (next (first-timeslot scheduler)))
      (unless (null (first-timeslot scheduler))
        (setf (prev (first-timeslot scheduler)) nil))
      (when (null (first-timeslot scheduler))
        (setf (last-timeslot scheduler) nil)))))

(defun next-time (&optional (scheduler *default-scheduler*))
  (when scheduler
    (time (first-timeslot scheduler))))

(defmethod schedule (object time &optional (scheduler *default-scheduler*))
  (let ((event (make-instance 'event :time time :object object)))
    (when (null scheduler)
      (setf *default-scheduler* (make-instance 'scheduler))
      (setf scheduler *default-scheduler*))
    (add-event event scheduler)))

#+debug 
(defmethod process :around ((slot timeslot))
  (format t "about to process timeslot: ~A at ~A~%"
          (sb-int:format-universal-time NIL (time slot))
          (sb-int:format-universal-time NIL (get-universal-time)))
  (call-next-method)
  (format t "done processing timeslot: ~A~%"           
          (sb-int:format-universal-time NIL (time slot)))
  (if (next-time)
      (format t "next timeslot: ~A~%"           
              (sb-int:format-universal-time NIL (next-time)))
      (format t "no next timeslot!~%")))

(defmethod process ((slot timeslot))
  (loop for event in (events slot)
     do (process event)))

(defvar *process-mutex* (sb-thread:make-mutex :name "process lock"))

(defmethod process ((event event))
  #-no-noctool-threads
  (handler-case 
      (sb-ext:with-timeout 10000
        (noc-thread (process (object event))))
    (sb-ext::timeout ()
      (warn "Timing out thread ~A~%" sb-thread:*current-thread*)))
  #+no-noctool-threads  
  (process (object event)))

(defmethod process :before ((event net.hexapodia.noctool-scheduler:event))
  (let ((obj (net.hexapodia.noctool-scheduler::object event)))
    (when (or (noctool::proxies (noctool::equipment obj)) (noctool:proxies obj))
      (push obj *network-updates-needed*))))

(define-symbol-macro threads (sb-thread:list-all-threads))

(defvar *scheduler-loop-control* nil "Set to NIL to terminate a running scheduler loop")

(defun scheduler-loop ()
  (setf *scheduler-loop-control* t)
      (loop while *scheduler-loop-control*
       do (let ((next (next-time)))
            (cond ((null next) (sleep 60))
                  ((<= next (get-universal-time))
                   (process (next-timeslot)))
                  (t (sleep (min 1 (- next (get-universal-time)))))))))

