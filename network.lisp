(in-package #:net.hexapodia.noctool-network)

(defclass connection ()
  ((peer :accessor peer :initarg :peer)
   (buffer :accessor buffer :initarg :buffer)
   (buffer-len :accessor buffer-len :initarg :buffer-len)
   (sock :reader sock :initarg :sock)
   (read-fun :accessor read-fun :initarg :read-fun)
   (nest-depth :accessor nest-depth :initarg :nest-depth)
   (fun-return :accessor fun-return :initarg :fun-return)
   (nonce1 :accessor nonce1 :initarg :nonce1)
   (nonce2 :accessor nonce2 :initarg :nonce2)
   (state :accessor state :initarg :state) 
   )
  (:default-initargs :nest-depth 0 :state :initial))

(defclass accept-event ()
  ())

(defclass peer-check-event ()
  ())

(defun find-peer (name)
  (gethash name *peers*))

(defun add-char (conn char)
  (when (null (vector-push char (buffer conn)))
    (vector-push-extend char (buffer conn) (buffer-len conn))
    (setf (buffer-len conn) (* 2 (buffer-len conn)))
    )
  t)


(declaim (ftype (function (connection) t)
		read-word read-string skip-whitespace read-open
		read-escaped skip-token))

(defun read-open (conn)
  (let ((char (read-char-no-hang (sock conn) nil)))
    (cond ((null char) nil)
	  ((char= char #\()
	   (setf (read-fun conn) #'read-word)
	   (add-char conn char)
	   (incf (nest-depth conn)))
	  (t t))))

(defun read-string (conn)
  (let ((char (read-char-no-hang (sock conn) nil)))
    (cond ((null char) nil)
	  ((char= char #\\)
	   (add-char conn char)
	   (setf (fun-return conn) #'read-string)
	   (setf (read-fun conn) #'read-escaped))
	  ((char= char #\")
	   (setf (read-fun conn) #'read-word)
	   (add-char conn char))
	  (t (add-char conn char)))))

(defun skip-whitespace (conn)
  (let ((char (read-char-no-hang (sock conn) nil)))
    (cond ((null char) nil)
	  ((member char '(#\Space #\Tab #\Return #\Newline)  :test #'char=)
	   nil) ;; We're just skipping, here
	  (t (unread-char char (sock conn))
	     (setf (read-fun conn) #'read-word)))))

(defun read-word (conn)
  (let ((char (read-char-no-hang (sock conn) nil)))
    (cond ((null char) nil)
	  ((char= char #\))
	   (add-char conn char)
	   (decf (nest-depth conn))
	   (not (zerop (nest-depth conn))))
	  ((char= char #\()
	   (add-char conn char)
	   (incf (nest-depth conn)))
	  ((char= char #\")
	   (add-char conn char)
	   (setf (read-fun conn) #'read-string))
	  ((member char '(#\Space #\Tab #\Return #\Newline) :test #'char=)
	   (add-char conn #\Space)
	   (setf (read-fun conn) #'skip-whitespace))
	  ((char= char #\#)
	   (add-char conn #\Space)
	   (setf (read-fun conn) #'skip-token))
	  ((char= char #\\)
	   (setf (fun-return conn) #'read-word)
	   (setf (read-fun conn) #'read-escaped)
	   (add-char conn char))
	  (t (add-char conn char)))))

(defun read-escaped (conn)
  (let ((char (read-char-no-hang (sock conn) nil)))
    (cond ((null char) nil)
	  (t (add-char conn char)
	     (setf (read-fun conn) (fun-return conn))))))

(defun skip-token (conn)
  (let ((char (read-char-no-hang (sock conn) nil)))
    (cond ((null char) nil)
	  ((member char '(#\Space #\Tab #\Return #\Newline) :test #'char=)
	   (setf (read-fun conn) #'skip-whitespace))
	  ((char= char #\()
	   (add-char conn char)
	   (incf (nest-depth conn))
	   (setf (read-fun conn) #'read-word))
	  ((char= char #\))
	   (setf (read-fun conn) #'read-word)
	   (decf (nest-depth conn))
	   (not (zerop (nest-depth conn))))
	  (t t))))

(defun parse-timestring (str)
  (or (ignore-errors
	    (when (and (= (length str) 15)
		       (= (position #\T str) 8))
	      (let ((year (parse-integer str :start 0 :end 4))
		    (month (parse-integer str :start 4 :end 6))
		    (day (parse-integer str :start 6 :end 8))
		    (hour (parse-integer str :start 9 :end 11))
		    (min (parse-integer str :start 11 :end 13))
		    (sec (parse-integer str :start 13 :end 15)))
		(encode-universal-time sec min hour day month year 0))))
      0))

(defun make-timestring ()
  (let ((now (get-universal-time)))
    (multiple-value-bind (sec min hour day mon year)
	(decode-universal-time now 0)
      (format nil "~4,'0d~2,'0d~2,'0dT~2,'0d~2,'0d~2,'0d"
	      year mon day hour min sec))))

(defun validate-timestring (str)
  (let ((then (parse-timestring str))
	(now (get-universal-time)))
    (<= 0 (- now then) 5)))

(defun make-signature (msg password)
  (let ((hmac (ironclad:make-hmac password :sha1)))
    (ironclad:update-hmac hmac (octetify msg))
    (encode-base64 (ironclad:hmac-digest hmac))))

(defgeneric send (conn format &rest args))

(defmethod send ((conn connection) format &rest args)
  (let ((stream (sock conn))
	(peer (peer conn)))
    (let ((msg (apply #'format nil format args)))
      (format stream "(message ~a ~s)" msg (make-signature msg (my-passwd peer)))
      (finish-output stream))))

(defmethod send ((peer remote-node) format &rest args)
  (apply #'send (conn peer) format args))

(defun protocol-error (conn)
  (send conn "(protocol-error)" nil))

(defun make-connection (socket)
  (make-instance 'connection :peer nil :sock socket
		 :buffer (make-array 20
				     :element-type 'character
				     :fill-pointer 0)
		 :buffer-len 20
		 :read-fun #'read-open))

(defun clear-connection (conn)
  (setf (fill-pointer (buffer conn)) 0)
  (setf (read-fun conn) #'read-open))

(defun connect (peer)
  (let ((stream (socket-stream (socket-connect (destination peer)
					       (dst-port peer)))))
    (let ((conn (make-connection stream)))
      (setf (conn peer) conn)
      (setf (peer conn) peer)
      (send conn "(iam ~s ~s ~s)"
	    (my-name peer)
	    (destination peer)
	    (make-timestring))
      (setf (state conn)
	    :sent-validation))))

(defun handle-read (conn)
  (loop for rv = (funcall (read-fun conn) conn)
	until (null rv))

  (when (and (zerop (nest-depth conn)) (> (fill-pointer (buffer conn)) 0))
    (prog1
	(copy-seq (buffer conn))
      (clear-connection conn))))

(defun find-node (name)
  (find name *peers* :test #'string= :key #'noctool::destination))

(defun start-listener (&key force (port *local-port*) (address *local-address*))
  (unless (and *incoming* (not force))
    (when *incoming*
      (socket-close *incoming*))
    (setf *incoming* (socket-listen address port))))

(defun check-accept (socket)
  (let ((new (socket-accept socket)))
    (push (make-connection (socket-stream new)) *connections*)))

(defun check-signature (conn message digest)
  (let ((peer (peer conn)))
    (let ((signature (make-signature message (remote-passwd peer))))
      (string= signature digest))))

(defun net-read (message)
  (let ((*package* *net-package*)
	(*read-suppress* nil)
	(*read-eval* nil)
	(*read-base* 10))
    (read-from-string message)))

(defun send-proxy-list (conn objects)
  (let ((objdata (loop for object in objects
		       collect (id object)
		       collect (list (class-name (class-of object)))
		       collect (name object))))
    (send conn "(class-list ~{(proxy-equipment (id ~a) (classes ~{~a~}) (name ~s))~})" objdata)))

(defun send-proxy-equipment (conn obj)
  (let ((monitor-list (loop for mon in (monitors obj)
			    collect (id mon)
			    collect (class-name (class-of mon))
			    collect (alert-level mon))))
  (send conn "(proxy-equipment (id ~a) (classes ~{~a~}) (name ~s) (monitors ~{(monitor (id ~a) (type ~a) (alert-level ~d))~}))" (id obj) (list (class-name (class-of obj))) (name obj) monitor-list)))

(defun send-graph (conn graph)
  (send conn "(proxy-graph (id ~s) (type ~a) ~{#(~{~a~})~})"
	(id graph)
	(graph-type graph)
	(serialize-data graph)))

(defun get-class (class-id)
  (or (gethash class-id *class-map*)
      (let ((package (symbol-package class-id))) 
	(cond ((eql package *net-package*)
	       (setf (gethash class-id *class-map*)
		     (find-class (find-symbol (symbol-name class-id)
					      *noctool-package*))))
	      ((eql package *noctool-package*)
	       (setf (gethash class-id *class-map*)
		     (find-class class-id)))
	      (t (error "Unknown class"))))))

(defun get-proxy (conn id)
  (unless *proxies*
    (setf *proxies* (make-hash-table :test 'equal)))
  (gethash (cons conn id) *proxies*))

(defun (setf get-proxy) (new conn id)
  (unless *proxies*
    (setf *proxies* (make-hash-table :test 'equal)))
  (setf (gethash (cons conn id) *proxies*) new))

(defun terminate-conn (conn)
  (setf (state conn) :terminated)
  (setf *connections* (delete conn *connections*)))

(defhandler request-proxy-class (request-proxy-class class-name)
  (let ((class (get-class class-name)))
    (let ((objects (loop for eq in *equipment*
			 if (eql (class-of eq) class)
			 collect eq)))

      (send-proxy-list conn objects))))

(defun make-proxy (peer kit)
  (make-instance 'net.hexapodia.noctool::proxy :remote-node peer :object kit))


(defhandler request-proxy-equipment (request-proxy-equipment eq-id)
  (let ((equipment (noctool::get-instance-by-id (string eq-id))))
    (when equipment
      (push (make-proxy (peer conn) equipment)
	    (noctool::proxies equipment))
      (send-proxy-equipment conn equipment))))

(defun handle-monitor (conn monitor-spec object)
  (when (eql (car monitor-spec) 'monitor)
    (let ((id nil)
	  (type nil)
	  (alert-val nil))
      (loop for (op opval) in (cdr monitor-spec)
	    do (case op
		 (id (setf id opval))
		 (type (setf type opval))
		 (alert-level (setf alert-val opval))))
      (let ((mon (make-instance type :id id :alert-level alert-val)))
	(push mon (monitors object))
	(setf (get-proxy conn id) mon)))))

(defhandler proxy-equipment (proxy-equipment &rest modifiers)
  (let ((classes nil)
	(monitors nil)
	(id nil)
	(name nil))
    (loop for (opname . opvals) in modifiers
	  do (case opname
	       (id (setf id (car opvals)))
	       (classes (setf classes opvals))
	       (monitors (setf monitors opvals))
	       (name (setf name (car opvals))))) 
    (let ((proxy (get-proxy conn id)))
      (let ((object (if proxy
			(object proxy)
			(make-instance (car classes) :id id :name name))))
	(unless proxy
	  (let ((proxy (make-proxy (peer conn) object)))
	    (setf (get-proxy conn id) proxy))
	  )
	(when (and monitors (null (monitors object)))
	  (push object *equipment*)
	  (loop for mon in monitors
		do (handle-monitor conn mon object)))

	(setf (gethash (cons (peer conn) :proxy-equipment) *reply-structure*)
	      object)
	))))

(defhandler class-list (class-list &rest equipment-list)
  (let ((data (loop for eq in equipment-list
		    collect (apply #'proxy-equipment conn eq))))
    (let ((peer (peer conn)))
      (setf (gethash (cons peer :list-class) *reply-structure*) data))))

(defhandler handle-protocol-error (protocol-error)
  (values :protocol-error conn))

(defhandler proxy-update (proxy-update &rest args)
  (let ((id (cadr (assoc 'id args)))
	(val (cadr (assoc 'alert-level args))))
    (let ((proxy (get-proxy conn id)))
      (setf (alert-level (object proxy)) val))))

(defhandler graph-update-handler (graph-update id measure)
  (unless  (and (eql (car id) 'id)
	     (eql (car measure) 'measure))
    (protocol-error conn)
    (error "Protocol error"))
  (let ((id (cadr id))
	(measure (cadr measure)))
    (let ((proxy (get-proxy conn id)))
      (when proxy 
	(noctool-graphs:add-value (object proxy) measure)))))

(defun delete-proxy (conn id)
  (let ((proxy (get-proxy conn id)))
    (when proxy
      (let ((kit (object proxy)))
	(setf (noctool:proxies kit)
	      (delete proxy (noctool:proxies kit)))
	(setf (get-proxy conn id) nil)))))

(defhandler delete-proxy-equipment (delete-proxy-equipment id)
  (delete-proxy conn id))

(defhandler delete-proxy-graph (delete-proxy-graph id)
  (delete-proxy conn id))


(defun handle-peer (conn)
  (let ((msg (handle-read conn)))
    (unless (null msg)
      (let* ((msg-start (position #\( msg :start 2))
	     (msg-end (position #\) msg :end (- (length msg) 2) :from-end t))
	     (transmission (net-read msg))
	     (message (subseq msg msg-start (1+ msg-end)))
	     (msg (nth 1 transmission))
	     (signature (nth 2 transmission))
	     (head (car msg)))
	(handler-case
	    (case (state conn)
	      ((:sent-validation :initial)
	       (case head
		 (iam (destructuring-bind
			    (iam remote-node me time)
			  msg
			(declare (ignorable iam))
			(let ((peer (find-peer remote-node)))
			  (when (and peer
				     (string= (my-name peer)
					      me))
			    (when (validate-timestring time)
			      (setf (peer conn) peer)
			      (cond ((check-signature conn message signature)
				     (setf (conn peer) conn)
				     (when (eql (state conn) :initial)
				       (send conn "(iam ~s ~s ~s)"
					     (my-name peer)
					     (destination peer)
					     (make-timestring)))
				     (setf (state conn) :validated))
				    (t (protocol-error conn)
				       (terminate-conn conn))))))))))
	      (:validated
	       (when (check-signature conn message signature)
		 (let ((handler (gethash head *handler-map*)))
		   (cond (handler (apply handler conn msg))
			 (t (protocol-error conn))))
		 )))
	  (error () (protocol-error conn)))))))


(defun read-remotely ()
  (loop for conn in *connections*
	do (handle-peer conn)))

(defmethod noctool-scheduler:process ((event accept-event))
  (check-accept *incoming*)
  (schedule event (1+ (get-universal-time))))

(defmethod noctool-scheduler:process ((event peer-check-event))
  (loop for conn in *connections*
	do (handle-peer conn))
  (schedule event (1+ (get-universal-time))))

(defun update-peers (monitor-list)
  (loop for mon in monitor-list
	do (let ((update (format nil "(proxy-update (id ~a) (alert-level ~a))"
				 (id mon) (alert-level mon))))
	     (loop for proxy in (noctool::proxies (noctool::equipment mon))
		   do (send (remote-node proxy) "~a" update)))))

(defmethod noctool-scheduler:process :before ((slot noctool-scheduler::timeslot))
  (let ((tmp nil))
    (shiftf tmp *network-updates-needed* nil)
    (sb-thread:make-thread (lambda () (update-peers tmp)))))
