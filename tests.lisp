(in-package #:net.hexapodia.noctool)


;; OK, this is *awesome* programming form, but... 
(defvar *ignore-errors* t)
(defmacro without-errors (error-form &body body)
  `(handler-case ,@body
     (error (c) 
       (if *ignore-errors*
           (progn
                  (warn "ignoring condition: ~A" c)
                  ,error-form)
           (error c)))))

(defmethod process :around ((monitor monitor))
  (let ((now (get-universal-time)))
    (unwind-protect
         (typecase monitor
           (noctool::ping-monitor
            (progn
              (call-next-method)
              (setf (noctool:last-updated monitor) (get-universal-time))))
           (t
            (if (host-pings monitor)
                (progn
                  (call-next-method)
                  (setf (noctool:last-updated monitor) (get-universal-time)))
                (progn
                  (let ((ping-monitor
                         (get-ping-monitor (noctool::equipment monitor))))
                    (warn "host ~A doesn't ping! failed: ~A over-rtt: ~A"
                          (noctool::name (noctool::equipment monitor))
                          (failed ping-monitor)
                          (over-rtt ping-monitor))
                    ;; this might not be needed
                    (if noctool-scheduler::*default-scheduler*
                        (unless (noctool-scheduler::find-object 
                                 ping-monitor
                                 noctool-scheduler::*default-scheduler*)
                          (warn "rescheduling ping monitor")
                          (schedule ping-monitor (+ (get-universal-time) 1)))
                        (warn "no default scheduler!")))))))
      (schedule monitor
                (max
                 (+ now
                    (ceiling 
                     (*
                      (interval monitor)
                      (if (>= (alert-level monitor) *alerting*) .5 1))))
                 (1+ now))))))

;; syntactic sugar on top of with-accessors to make it more like with-slots
(defmacro with-slot-accessors ((&rest accessors) instance &body body)
  (let ((acc-list (mapcar (lambda (x) (if (listp x) x `(,x ,x))) accessors)))
    `(with-accessors ,acc-list ,instance ,@body)))

(defmethod process ((monitor ping-monitor))
  (without-errors NIL
    (with-slot-accessors (equipment ping-count ping-interval max-rtt alert-level max-fail graph) monitor
      (let ((kit equipment))
        (let ((data (make-ping (name kit) :interval ping-interval :count ping-count)))
          (let ((failed (count -1.0 data :test #'=))
                (over-rtt (count max-rtt data :test #'<)))
            (setf alert-level
                  (decay-alert alert-level
                               (+ (if (>= failed max-fail)
                                      *alerting*
                                      0)
                                  (* 10 failed)
                                  (* 5 over-rtt)))
                  (failed monitor) failed
                  (over-rtt monitor) over-rtt)
            (when graph
              (add-value graph (reduce #'max data)))))))))

(defgeneric process-disk (monitor host))
(defgeneric process-df (host pty))

(defun parse-gobble (list)
  (let* ((pos 0)
	(first (loop for element in list
		     for lpos from 1
		     while (not (parse-integer element :junk-allowed t))
		     do (setf pos lpos)
		     collect element)))
    (cond ((zerop pos) list)
	  (t (cons (format nil "~{~a~^ ~}" first) (subseq list pos))))))

(defun parse-df (pty)
  "parse output from df from the given pty"
  (read-pty-line pty) ;; throw away informational line
  (let ((ret NIL))
    (loop for line = (read-pty-line pty nil)
       for split = (parse-gobble (split-line line))
       with last = NIL
       for len = (+ (length split) (length last))
       while line
       if (cl-ppcre::scan "(^$|^Filesystem)" line)
       do
         (setf len 0)
       else if (> 6 len)
       do
         (setf last split)
       else
       do
         (let ((result (nconc last split)))
           (if (= (length result) 8)
               (push (cons (format NIL "~A-~A" (car result) (cadr result))
                           (cdddr result)) ret)
               (push result ret))
           (setf last NIL)))
    ret))

(defmethod process-df (host pty)
  (declare (ignore host))
  (without-errors NIL
    (parse-df pty)))

(defmethod process-disk ((monitor disk-container) (host linux-host))
  (without-errors NIL
    (with-pty (pty (make-ssh-command "df -P" (address host) (username host)))
      ;; Process disk block usage
      (let ((parsed (parse-df pty)))
        (loop for split in parsed
           do
             (destructuring-bind (device disk used free percent mount)
                 split
               (declare (ignore percent))
               (unless (member device (ignore-list monitor) :test #'string=)
                 (let ((disk (parse-integer disk))
                       (used (parse-integer used))
                       (free (parse-integer free)))
                   (let ((platter (find device (disk-list monitor)
                                        :test #'string=
                                        :key 'device)))
                     (unless platter
                       (setf platter
                             (make-instance 'disk-monitor
                                            :device device
                                            :equipment (equipment monitor)
                                            :disk-max disk
                                            :interval (interval monitor)
                                            :parent monitor
                                            :last-updated (get-universal-time)))
                       (add-graphs platter)
                       (push platter
                             (disk-list monitor)))
                     (when platter
                       (setf (mountpoint platter) mount)
                       (setf (disk-free platter) free)
                       (setf (disk-max platter) disk)
                       (add-value (disk-graph platter) used)
                       (setf (last-updated platter) (get-universal-time))
                       (setf (last-seen platter) (get-universal-time))
                       (let ((percent (if (= 0 disk) 100 (* 100 (/ used disk)))))
                         (setf (alert-level platter)
                               (decay-alert
                                (alert-level platter)
                                (cond ((<= (disk-percent platter) percent)
                                 *alerting*)
                                      ((<= (* 0.9 (disk-percent platter))
                                           percent)
                                       *warning*)
                                      (t 0))))))))))))
      ;; Check for disks that have not been probed for roughly two intervals
      (loop for platter in (disk-list monitor)
         with threshold = (- (get-universal-time)
			     (max-since-seen platter))
         do (when (<= (last-updated platter) threshold)
              (setf (alert-level platter)
                    *alerting*))))))

(defun parse-ps (pty)
  "parse output from ps from the given pty"
  (read-pty-line pty) ;; throw away informational line
    (loop for line = (read-pty-line pty nil)
       while line
       collect
         (cl-ppcre::register-groups-bind (uid pid ppid cmd args) 
             ("^(\\S+)\\s+(\\S+)\\s+(\\S+)\\s+(\\S+)(?:\\s+(.*))?\\\r" line)
           (list uid pid ppid cmd args))))

(defgeneric find-process (process-monitor processes))

(defmethod find-process ((mon process-monitor) procs)
  (loop for proc in procs
     nconc
       (destructuring-bind (owner pid ppid name args) proc
         (when (and 
                (if (owner mon) (equal (owner mon) owner) t)
                (if (pid mon) (equal (pid mon) pid) t)
                (if (ppid mon) (equal (ppid mon) ppid) t)
                (if (name mon) (equal (name mon) name) t)
                (if (args mon) (equal (args mon) args) t))
           (list proc)))))


(defmethod process ((monitor process-container))
  (without-errors NIL
    (process-procs monitor (equipment monitor))))

(defmethod process-procs ((monitor process-container) (host linux-host))
  (with-slot-accessors (processes) monitor
    (without-errors NIL
      (with-pty (pty (make-ssh-command "ps -o user,pid,ppid,command -ax"
                                       (address host) (username host)))
        ;; Process processes
        (let ((found-procs (parse-ps pty)))
          (loop for proc in (processes monitor)
             as found = (find-process proc found-procs)
             as len = (length found)
             do
               (setf (found-procs proc) len)
             when (or (and (max-procs proc)
                           (> len (max-procs proc)))
                      (and (min-procs proc)
                           (< len (min-procs proc))))
             do 
               (format t "bad! ~A expected between ~A and ~A - found ~A~%" (name proc) (max-procs proc) (min-procs proc) len)
               (setf (alert-level proc) *alerting*)
             else
             do
               (setf (alert-level proc) 0)))))))

(defmethod process ((monitor disk-container))
  (without-errors NIL
    (process-disk monitor (equipment monitor))))

(defmethod process ((monitor load-monitor))
  (without-errors NIL
    (with-pty (pty (make-ssh-command "uptime" (address (equipment monitor)) (username (equipment monitor))))
      (let ((data (split-line (string-trim '(#\Space #\Return #\Newline)
                                              (read-line pty)))))
        (let ((loads (cdr (member "average" data :test #'search))))
          (let ((now-load (read-from-string (or (nth 0 loads) "-1"))))
            (let ((new-alert
                   (cond ((< now-load 0) *alerting*)
                         ((< now-load (low-water monitor)) 0)
                         ((<= (low-water monitor) now-load (high-water monitor))
                          (+ *warning* (* (- now-load (low-water monitor))
                                          (/ (- *alerting* *warning*)
                                             (- (high-water monitor)
                                                (low-water monitor))))))
                         (t *alerting*))))
              (setf (alert-level monitor) (decay-alert (alert-level monitor)
                                                       (round new-alert))))
            (add-value (graph-1  monitor) now-load)
            (add-value (graph-5  monitor) (read-from-string (or (nth 1 loads) "-1")))
            (add-value (graph-10 monitor) (read-from-string (or (nth 2 loads) "-1")))))))))

(defmethod process ((monitor tcp-monitor))
  (when (and (sent-data monitor)
	     (match-data monitor))
    (let ((host (address (equipment monitor)))
	  (new-alert *alerting*)
	  (sent-data (sent-data monitor))
	  (started (get-internal-real-time))
	  (stopped nil))
      (unwind-protect
	  (let ((stream (socket-stream (socket-connect host (port monitor)))))
	    (when stream
	      (unwind-protect
		  (progn
		    (format stream "~a" sent-data)
		    (finish-output stream)
		    (let ((data (read-until-eof stream)))
		      (if (match (match-data monitor) data)
			  (setf new-alert 0))))
		(close stream))))
	(setf stopped (get-internal-real-time)))
      (format t "DEBUG: Time diff is ~a~%" (- stopped started))
      (when (> (- stopped started)
	       (* (delay monitor)
		  internal-time-units-per-second))
	(incf new-alert *warning*))
      (setf (alert-level monitor)
	    (decay-alert (alert-level monitor) new-alert)))))

(defmethod process-snmp-interface (interface ifhash session)
  (let ((if-ix (gethash (name interface) ifhash))
	(new-alert 0))
    (labels ((fetch (varname)
	       (car (snmp:snmp-get session
			      (list (format nil "~a.~d" varname if-ix))))))
      (let ((octets-in (fetch "ifInOctets"))
	    (octets-out (fetch "ifOutOctets"))
	    (errors-in (fetch "ifInErrors"))
	    (errors-out (fetch "ifOutErrors"))
	    (discards-in (fetch "ifInDiscards"))
	    (discards-out (fetch "ifOutDiscards"))
	    (admin-status (fetch "ifAdminStatus"))
	    (oper-status (fetch "ifOperStatus")))
	(add-value (in-octets interface)
		   (slot-value octets-in 'asn.1::value))
	(add-value (out-octets interface)
		   (slot-value octets-out 'asn.1::value))
	(add-value (in-errors interface)
		   (slot-value errors-in 'asn.1::value))
	(add-value (out-errors interface)
		   (slot-value errors-out 'asn.1::value))
	(add-value (in-discards interface)
		   (slot-value discards-in 'asn.1::value))
	(add-value (out-discards interface)
		   (slot-value discards-out 'asn.1::value))

	(when (oper-status interface)
	  (unless (eql (oper-status interface) oper-status)
	    (setf new-alert *alerting*)))
	(setf (oper-status interface) oper-status)
	
	(when (admin-status interface)
	  (unless (eql (admin-status interface) admin-status)
	    (setf new-alert *alerting*)))
	(setf (admin-status interface) admin-status)
	
	(setf (alert-level interface)
	      (decay-alert (alert-level interface) new-alert))))))
      


(defmethod process ((monitor snmp-container))
  (let ((host (address (equipment monitor))))
    (snmp:with-open-session (snmp-session host :version (version monitor) :community (public monitor))
      (let ((ifnames (snmp:snmp-walk snmp-session '("ifDescr")))
	    (ifhash (make-hash-table :test #'equalp)))
	(when (eql :all (interfaces monitor))
	  (mapcar #'cadr (car ifnames)))
	(loop for (obj name) in (car ifnames)
	     do (setf (gethash name ifhash) (slot-value obj 'asn.1::value)))
	(loop for int in (interfaces monitor)
	     do (process-snmp-interface int ifhash snmp-session))
	))))
