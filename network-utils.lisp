(in-package :net.hexapodia.noctool-network)

(defmacro defhandler (name (proto-sym &rest llist) &body body)
  "Define a handler for VALIDATED part of a connection, it will add an
argument called CONN as the very first element. This is the connection
that the request came in over. Validation of the signature is done before
any dispatching. The first argument should be the protocol symbol to dispatch
on, it is declared as IGNORABLE, the rest of the parameters should map
to the protocol request."
  (setf (gethash proto-sym *handler-map*) name)
  `(defun ,name (conn ,proto-sym ,@llist)
    (declare (ignorable ,proto-sym))
    ,@body))

(defun pick (object sequence &key (test #'eql) (key #'identity))
  (loop for pos = (position object sequence :test test :key key) then (position object sequence :test test :key key :start (1+ pos))
	until (null pos)
	collect (elt sequence  pos)))
