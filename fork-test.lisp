
(defmacro with-pty (ptyspec &body body)
  "Run a command with a PTY bound. If the ptyspec is (run-command ...), the
pty will be bound to the symbol PTY, otherwise the ptyspec is expected to be
a list (ptyvar command)"
  (let ((pty (if (eql (car ptyspec) 'run-command)
                 'pty
               (car ptyspec)))
        (command (if (eql (car ptyspec) 'run-command)
                     ptyspec
                   (cadr ptyspec))))
    `(let ((,pty ,command))
       (unwind-protect
           (progn ,@body)
         (close ,pty)))))

(defun worker (id lines)
  (let ((id-string (format nil "~4,'0d" id)))
    (with-pty (pty (sb-ext:process-pty (sb-ext:run-program "/usr/bin/env" (list "yes" id-string) :wait nil :pty t)))
      (loop for line = (read-line pty nil nil)
	    for count from 0 below lines
	    as chop = (subseq line 0 4) ; get rid of ^M
	    while line
	    do (progn
		 (unless (string= chop id-string)
		   (format t "Thread ~d, expected ~a, saw ~a~%" id id-string chop))
		 (sleep (* 0.1 (random 10))))))))

(defun main (workers &optional (line-count 100))
  (loop for n from 1 to workers
	collect (sb-thread:make-thread #'(lambda () (worker n line-count)))))
