(in-package :cl-user)

(defpackage #:noctool-ssh
  (:use #:cl #:cffi)
  (:export #:ssh-connect #:ssh-read #:ssh-write))
