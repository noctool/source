(in-package #:noctool)

(defvar *todo-list* nil)
(defvar *todo-mutex* (sb-thread:make-mutex :name "todo-mutex"))

(defmethod process ((todo todo-item))
  (sb-thread:with-mutex (*todo-mutex*)
    (push todo *todo-list*))
  (setf (last-dequeued todo) (get-universal-time)))
			      

(defun enqueue-todo (todo)
  (let ((now (get-universal-time)))
    (setf (last-scheduled todo) now)
    (sb-thread:with-mutex (*todo-list*)
      (setf *todo-list* (delete todo *todo-list*))
      (schedule todo (+ now (interval todo))))))

(defgeneric pass (item))
(defgeneric fail (item))

(defmethod pass ((todo todo-item))
  (setf (last-status todo) 'ok)
  (enqueue-todo todo))

(defmethod fail ((todo todo-item))
  (setf (last-status todo) 'fail)
  (enqueue-todo todo))

(defun new-todo-item (thing description &key (interval 86400))
  "Create and enqueue a new todo item for THING, with DESCRIPTION,
to be re-scheduled for INTERVAL seconds after a pass or fail."
  (let ((todo (make-instance 'todo-item
			     :thing thing
			     :description description
			     :interval interval
			     :last-status 'unknown
			     :last-dequeued 0)))
    (enqueue-todo todo)
    todo))
