(in-package #:net.hexapodia.noctool)

;;(default-monitor unix-host cpu-monitor)
(default-monitor unix-host load-monitor)
(default-monitor unix-host disk-container)

(default-monitor equipment ping-monitor)
