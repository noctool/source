(in-package :net.hexapodia.noctool-network)

(defvar *incoming* nil)
(defvar *local-address* "localhost")
(defvar *local-port* 11378)
(defvar *net-package* (find-package :net.hexapodia.noctool-network))
(defvar *stop-accept-loop* nil "Variable to control if we need to stop the accept loop")
(defvar *connections* nil)
(defvar *class-map* (make-hash-table))

(defvar *handler-map* (make-hash-table))

(defvar *proxies* (make-hash-table :test 'equal))

(defvar *reply-structure* (make-hash-table :test 'equal))

(declaim (ftype (function (t t t)) update-graph))
