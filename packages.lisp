(in-package :cl-user)

(defpackage #:net.hexapodia.noctool-scheduler
  (:nicknames #:noctool-scheduler)
  (:use #:cl)
  (:shadow #:time)
  (:export #:schedule #:next-timeslot #:events #:process #:next-time #:*network-updates-needed* #:event))

(defpackage #:net.hexapodia.noctool-graphs
  (:nicknames #:noctool-graphs)
  (:use :cl
	#+sbcl :sb-mop)
  (:export #:graph-type #:serialize-data #:show #:make-graph #:meter-graph #:gauge-graph #:avg-graph #:max-graph #:add-value #:fixed-graph-display #:percentile-graph-display #:add-graphs #:add-graph-info #:interval #:store))

(defpackage #:net.hexapodia.noctool
  (:nicknames #:noctool)
  (:use #:cl #:usocket #:net.hexapodia.noctool-scheduler #:net.hexapodia.noctool-graphs
	#+sbcl :sb-mop)
  (:export
   #:post-config-fixup #:proxies #:*proxies* #:*peers* #:*equipment* #:*views* #:*noctool-package* #:id #:last-updated #:unix-host #:linux-host #:cpu-monitor #:load-monitor #:ping-monitor #:remote-node #:decode-base64 #:encode-base64 #:octetify #:destination #:alert-level #:conn #:monitors #:my-name #:my-passwd #:serialize-data #:remote-node #:dst-port #:remote-passwd #:name #:graph-type #:object #:disk-container #:make-snmp-interface
   ))

(defpackage #:net.hexapodia.noctool-config
  (:nicknames #:noctool-config)
  (:use #:net.hexapodia.noctool #:net.hexapodia.noctool-graphs #:cl)
  (:shadow #:load #:interfaces)
  (:export #:cluster #:ping #:load #:machine #:user #:ip #:ssh-port #:disk #:disks #:disk-ignore #:procs #:proc #:local-password #:local-hostname #:peer #:with-format #:snmp #:public #:private #:version #:interfaces))

(defpackage #:net.hexapodia.noctool-network
  (:nicknames #:noctool-network)
  (:use #:net.hexapodia.noctool #:cl #:usocket #:net.hexapodia.noctool-scheduler)
  (:export #:graph-update #:connect #:send #:disconnect #:start-listener #:list-class #:subscribe #:unsubscribe))

;; a package to hold gentemp'd symbols
(defpackage #:net.hexapodia.noctool-symbols
  (:nicknames #:noctool-symbols))